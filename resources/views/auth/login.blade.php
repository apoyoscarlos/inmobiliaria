<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title>Login</title>
	<meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
	<link rel="icon" href="{{ asset('template/img/icon.ico" type="image/x-icon') }}"/>

	<!-- Fonts and icons -->
	<script src="{{ asset('template/js/plugin/webfont/webfont.min.js') }}"></script>
	
	
	<!-- CSS Files -->
	<link rel="stylesheet" href="{{ asset('template/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('template/css/atlantis.css') }}">
    
    <!-- CSS Propios -->
	<link rel="stylesheet" href="{{ asset('css/manuel.css') }}">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" integrity="sha512-iBBXm8fW90+nuLcSKlbmrPcLa0OT92xO1BIsZ+ywDWZCvqsWgccV3gFoRBv0z+8dLJgyAHIhR35VZc2oM/gI1w==" crossorigin="anonymous" />
</head>
<body class="login">
	<div class="wrapper wrapper-login">
		<div class="container container-login animated fadeIn">
			<form action="/auth/login" method="POST" id="frmLogin">
				@csrf
				<h3 class="text-center">Bienvenido</h3>
				<div class="login-form">
					@if($errors->any())
						<h5 class="text-center text-danger" id="errorLogin" >{{ $errors->first() }}</h5>
					@endif

					<div class="form-group form-floating-label">
						<input id="username" name="username" type="text" class="form-control input-border-bottom" required value="{{old('username')}}">
						<label for="username" class="placeholder">Usuario</label>
						<span class="error" id="errorUsuario" style="display: none;"></span>
					</div>
					<div class="form-group form-floating-label">
						<input id="password" name="password" type="password" class="form-control input-border-bottom" required>
						<label for="password" class="placeholder">Contraseña</label>
						<span class="error" id="errorPassword" style="display: none;">El campo contraseña es requerida.</span>
						<div class="show-password">
							<i class="icon-eye"></i>
						</div>
					</div>
					
					<div class="form-action mb-3">
						<button class="btn btn-primary btn-rounded btn-login" id="btnLogin" style="width: 100%" onclick="login()">
							Iniciar Sesión
						</button>
					</div>
					<div class="login-account">						
						<button id="show-signup" class="btn btn-link"> Recuperar contraseña </button>
					</div>
				</div>
			</form>
		</div>

		<div class="container container-signup animated fadeIn">
			<h3 class="text-center">Recuparar Contraseña</h3>
			<div class="login-form">
				<div class="form-group form-floating-label">
					<input id="txtCorreo" name="txtCorreo" type="text" class="form-control input-border-bottom" required>
					<label for="txtCorreo" class="placeholder">Correo</label>
					<span class="error" id="errorCorreo" style="display: none;"></span>
				</div>
				<div class="form-group form-floating-label">
					<input id="txtConfirmacion" name="txtConfirmacion" type="text" class="form-control input-border-bottom" required>
					<label for="txtConfirmacion" class="placeholder">Confirmar correo</label>
					<span class="error" id="errorConfirmacion" style="display: none;"></span>
				</div>
				
				<div class="form-action">
					<button  id="show-signin" class="btn btn-danger btn-link btn-login mr-3"> Cancel</button>
					<button  id="btnRecuperar" class="btn btn-primary btn-rounded btn-login" onclick="recuperar()"> Recuperar</button>
				</div>
			</div>
		</div>
	</div>
	<script src="{{ asset('template/js/core/jquery.3.2.1.min.js') }}"></script>
	<script src="{{ asset('template/js/plugin/jquery-ui-1.12.1.custom/jquery-ui.min.js') }}"></script>
	<script src="{{ asset('template/js/core/popper.min.js') }}"></script>
	<script src="{{ asset('template/js/core/bootstrap.min.js') }}"></script>
	<script src="{{ asset('template/js/atlantis.min.js') }}"></script>
	
	<!-- Sweet Alert -->
	<script src="{{ asset('template/js/plugin/sweetalert/sweetalert.min.js') }}"></script>
	<script>
		$(document).ready(function(){     
			$("#password").keypress(function(e) {
				if(e.which == 13) {
					login();
				}
			});
		});

		function login(){
			$("#errorLogin").hide();		
			if(validaciones() !== true )
				return;

			document.getElementById("frmLogin").submit();
		}

		function recuperar(){
			let success = true;
			$("#errorCorreo").hide();
			$("#errorConfirmacion").hide();
			$("#btnRecuperar").text("");
			$("#btnRecuperar").append(`<i class="fas fa-spinner fa-pulse fa-2x" id='cargando'></i>`);
			
           	if($("#txtCorreo").val() == ""){
				success = false;
				$("#errorCorreo").text("El campo correo es requerido.");
				$("#errorCorreo").show();
		   	}else{
				const strongRegex = /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/;
				if( !strongRegex.test($("#txtCorreo").val())) {
					success = false;
					$("#errorCorreo").text("El campo debe de ser un correo.");
					$("#errorCorreo").show();
				}
			}				

			if($("#txtConfirmacion").val() == ""){
				success = false;
				$("#errorConfirmacion").text("El campo correo es requerido.");
				$("#errorConfirmacion").show();
		   	}else{
				const strongRegex = /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/;
				if( !strongRegex.test($("#txtConfirmacion").val())) {
					success = false;
					$("#errorConfirmacion").text("El campo debe de ser un correo.");
					$("#errorConfirmacion").show();
				}else{
					if($("#txtConfirmacion").val() != $("#txtCorreo").val()){
						success = false;
						$("#errorConfirmacion").text("La campos no son iguales.");
						$("#errorConfirmacion").show();
					}
				}
			}			
			
			if(!success){
				$("#btnRecuperar").text("Recuperar");
				$("#cargando").remove();
				return;
			}

			//Falta api para el reseteo de contraseña
			$("#username").val("");
			$("#password").val("");
			$("#txtConfirmacion").val("");
			$("#txtCorreo").val("");
			
			
			setTimeout(() => {
				swal({
					title: 'Operacion exitosa',
					text: 'Se ha enviado la nueva contraseña al correo indicado.',
					icon: 'success',
					type: 'success',
					showConfirmButton: false,
					timer: 2300
				});
				setTimeout(() => {
					document.getElementById("show-signin").click();
				}, 1000);
			}, 1500);
		}

		function validaciones(){
			let success = true;
			$("#errorUsuario").hide();
			$("#errorPassword").hide();
			$("#btnLogin").text("");
			$("#btnLogin").append(`<i class="fas fa-spinner fa-pulse fa-2x" id="cargando"></i>`);

           	if($("#username").val() == ""){
				success = false;
				$("#errorUsuario").text("El campo usuario es requerido.");
				$("#errorUsuario").show();
		   	}else{
				const strongRegex = /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@\”]{2,})$/;
				if( !strongRegex.test($("#username").val())) {
					success = false;
					$("#errorUsuario").text("El usuario debe de ser un correo.");
					$("#errorUsuario").show();
				}
			}				

			if($("#password").val() == ""){
				success = false;
				$("#errorPassword").show();
				
			}

			if(!success){
				$("#btnLogin").text("Iniciar Sesión");
				$("#cargando").remove();
			}

            return success;
        }
	</script>
</body>
</html>
<table>
    <thead>
        <tr>
            @if ($rol == 'admin')
                <th>Cliente</th>
            @endif

            <th>Clave</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Estatus</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($datos as $item)
            <tr>
                @if ($rol == 'admin')
                    <td>{{ $item->nom_cliente. " ". $item->cli_apellido }}</td>
                @endif
                <td>{{ $item->clave }}</td>
                <td>{{ $item->nombre }}</td>
                <td>{{ $item->descripcion }}</td>
                <td>{{ $item->activo == 0 ? "Inactivo" : "Activo" }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
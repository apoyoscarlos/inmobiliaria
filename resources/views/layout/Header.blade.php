<div class="main-header" data-background-color="dark2" custom-color="#1f1e2e">
    <div class="nav-top py-lg-3">
        <div class="container d-flex flex-row">
            <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon">
                    <i class="icon-menu"></i>
                </span>
            </button>
            <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
            <!-- Logo Header -->
            <a href="index.html" class="logo d-flex align-items-center">
                <img src="{{ asset('template/img/logo.svg') }}" alt="navbar brand" class="navbar-brand">
            </a>
            <!-- End Logo Header -->
            
            <!-- Navbar Header -->
            <nav class="navbar navbar-header-left navbar-expand-lg p-0" data-background-color="dark2">
                <ul class="navbar-nav page-navigation pl-md-3">
                    <h3 class="title-menu d-flex d-lg-none">
                        Menu
                        <div class="close-menu"> <i class="flaticon-cross"></i></div>
                    </h3>
                    <li @if(Request::path() == 'inmuebles'){!! "class='nav-item active'" !!} @else {!! "class='nav-item'" !!} @endif>
                        <a class="nav-link" href="{{ route('inmuebles') }}">
                            Inmuebles
                        </a>
                    </li>
                    <li @if(Request::path() == 'usuarios'){!! "class='nav-item active'" !!} @else {!! "class='nav-item'" !!} @endif>
                        <a class="nav-link" href="{{ route('usuarios') }}">
                            Usuarios
                        </a>
                    </li>
                    <li @if(Request::path() == 'paquetes'){!! "class='nav-item active'" !!} @else {!! "class='nav-item'" !!} @endif>
                        <a class="nav-link" href="{{ route('paquetes') }}">
                            Paquetes
                        </a>
                    </li>
                    <li @if(str_contains(Request::path(),'catalogos/') == 1)  
                                {!!"class='nav-item dropdown show'"!!} 
                            @else 
                                {!!"class='nav-item dropdown'"!!} 
                            @endif>
                        <a class="nav-link dropdown-toggle" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Catálogos
                        </a>
                        <div class="dropdown-menu animated fadeIn" aria-labelledby="navbarDropdown">
                            <a @if (Request::path() == 'catalogos/orientaciones') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('cat-orientaciones') }}">Orientaciones</a>
                            <a @if (Request::path() == 'catalogos/unidadDeMedida') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('cat-unidad-de-medidas') }}">Unidades de Medida</a>
                            <a @if (Request::path() == 'catalogos/areas') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('cat-areas') }}">Areas</a>
                            <a @if (Request::path() == 'catalogos/tipos') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('cat-tipos') }}">Tipos</a>
                            <a @if (Request::path() == 'catalogos/niveles') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('cat-niveles') }}">Niveles</a>
                            <a @if (Request::path() == 'catalogos/usos') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('cat-usos') }}">Usos</a>
                            <a @if (Request::path() == 'catalogos/lineas') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('cat-tipo-lineas') }}">Tipo Linea</a>
                            <a @if (Request::path() == 'catalogos/clasificaciones') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('cat-clasificaciones') }}">Clasificaciones</a>
                            <a @if (Request::path() == 'catalogos/colindancias') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('cat-colindancia') }}">Colindancias</a>
                            <a @if (Request::path() == 'catalogos/subscripciones') {!! "class='dropdown-item active'" !!} @else {!! "class='dropdown-item'" !!} @endif href="{{ route('subscripcion') }}">Subscripciones</a>
                        </div>
                    </li>
                </ul>
            </nav>
            <nav class="navbar navbar-header navbar-expand-lg p-0" data-background-color="custom" custom-color="#1f1e2e">
                <div class="container-fluid p-0">
                    <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                        <li class="nav-item dropdown hidden-caret">
                            <a class="nav-link dropdown-toggle" href="#" id="messageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-envelope"></i>
                            </a>
                            <ul class="dropdown-menu messages-notif-box animated fadeIn" aria-labelledby="messageDropdown">
                                <li>
                                    <div class="dropdown-title d-flex justify-content-between align-items-center">
                                        Messages
                                        <a href="#" class="small">Mark all as read</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="message-notif-scroll scrollbar-outer">
                                        <div class="notif-center">
                                            <a href="#">
                                                <div class="notif-img">
                                                    <img src="../assets/img/jm_denis.jpg" alt="Img Profile">
                                                </div>
                                                <div class="notif-content">
                                                    <span class="subject">Jimmy Denis</span>
                                                    <span class="block">
                                                        How are you ?
                                                    </span>
                                                    <span class="time">5 minutes ago</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="notif-img">
                                                    <img src="../assets/img/chadengle.jpg" alt="Img Profile">
                                                </div>
                                                <div class="notif-content">
                                                    <span class="subject">Chad</span>
                                                    <span class="block">
                                                        Ok, Thanks !
                                                    </span>
                                                    <span class="time">12 minutes ago</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="notif-img">
                                                    <img src="../assets/img/mlane.jpg" alt="Img Profile">
                                                </div>
                                                <div class="notif-content">
                                                    <span class="subject">Jhon Doe</span>
                                                    <span class="block">
                                                        Ready for the meeting today...
                                                    </span>
                                                    <span class="time">12 minutes ago</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="notif-img">
                                                    <img src="{{ asset('template/img/profile.jpg') }}" alt="Img Profile">
                                                </div>
                                                <div class="notif-content">
                                                    <span class="subject">Talha</span>
                                                    <span class="block">
                                                        Hi, Apa Kabar ?
                                                    </span>
                                                    <span class="time">17 minutes ago</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a class="see-all" href="javascript:void(0);">See all messages<i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown hidden-caret">
                            <a class="nav-link dropdown-toggle" href="#" id="notifDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fa fa-bell"></i>
                                <span class="notification">4</span>
                            </a>
                            <ul class="dropdown-menu notif-box animated fadeIn" aria-labelledby="notifDropdown">
                                <li>
                                    <div class="dropdown-title">You have 4 new notification</div>
                                </li>
                                <li>
                                    <div class="notif-scroll scrollbar-outer">
                                        <div class="notif-center">
                                            <a href="#">
                                                <div class="notif-icon notif-primary"> <i class="fa fa-user-plus"></i> </div>
                                                <div class="notif-content">
                                                    <span class="block">
                                                        New user registered
                                                    </span>
                                                    <span class="time">5 minutes ago</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="notif-icon notif-success"> <i class="fa fa-comment"></i> </div>
                                                <div class="notif-content">
                                                    <span class="block">
                                                        Rahmad commented on Admin
                                                    </span>
                                                    <span class="time">12 minutes ago</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="notif-img">
                                                    <img src="../assets/img/profile2.jpg" alt="Img Profile">
                                                </div>
                                                <div class="notif-content">
                                                    <span class="block">
                                                        Reza send messages to you
                                                    </span>
                                                    <span class="time">12 minutes ago</span>
                                                </div>
                                            </a>
                                            <a href="#">
                                                <div class="notif-icon notif-danger"> <i class="fa fa-heart"></i> </div>
                                                <div class="notif-content">
                                                    <span class="block">
                                                        Farrah liked Admin
                                                    </span>
                                                    <span class="time">17 minutes ago</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <a class="see-all" href="javascript:void(0);">See all notifications<i class="fa fa-angle-right"></i> </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item dropdown hidden-caret">
                            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                                <i class="fas fa-layer-group"></i>
                            </a>
                            <div class="dropdown-menu quick-actions quick-actions-info animated fadeIn">
                                <div class="quick-actions-header">
                                    <span class="title mb-1">Quick Actions</span>
                                    <span class="subtitle op-8">Shortcuts</span>
                                </div>
                                <div class="quick-actions-scroll scrollbar-outer">
                                    <div class="quick-actions-items">
                                        <div class="row m-0">
                                            <a class="col-6 col-md-4 p-0" href="#">
                                                <div class="quick-actions-item">
                                                    <div class="avatar-item bg-danger rounded-circle">
                                                        <i class="far fa-calendar-alt"></i>
                                                    </div>
                                                    <span class="text">Calendar</span>
                                                </div>
                                            </a>
                                            <a class="col-6 col-md-4 p-0" href="#">
                                                <div class="quick-actions-item">
                                                    <div class="avatar-item bg-warning rounded-circle">
                                                        <i class="fas fa-map"></i>
                                                    </div>
                                                    <span class="text">Maps</span>
                                                </div>
                                            </a>
                                            <a class="col-6 col-md-4 p-0" href="#">
                                                <div class="quick-actions-item">
                                                    <div class="avatar-item bg-info rounded-circle">
                                                        <i class="fas fa-file-excel"></i>
                                                    </div>
                                                    <span class="text">Reports</span>
                                                </div>
                                            </a>
                                            <a class="col-6 col-md-4 p-0" href="#">
                                                <div class="quick-actions-item">
                                                    <div class="avatar-item bg-success rounded-circle">
                                                        <i class="fas fa-envelope"></i>
                                                    </div>
                                                    <span class="text">Emails</span>
                                                </div>
                                            </a>
                                            <a class="col-6 col-md-4 p-0" href="#">
                                                <div class="quick-actions-item">
                                                    <div class="avatar-item bg-primary rounded-circle">
                                                        <i class="fas fa-file-invoice-dollar"></i>
                                                    </div>
                                                    <span class="text">Invoice</span>
                                                </div>
                                            </a>
                                            <a class="col-6 col-md-4 p-0" href="#">
                                                <div class="quick-actions-item">
                                                    <div class="avatar-item bg-secondary rounded-circle">
                                                        <i class="fas fa-credit-card"></i>
                                                    </div>
                                                    <span class="text">Payments</span>
                                                </div>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link quick-sidebar-toggler">
                                <i class="fa fa-th"></i>
                            </a>
                        </li>
                        <li class="nav-item dropdown hidden-caret">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#" aria-expanded="false">
                                <span class="d-flex align-items-center">
                                    <div class="avatar-sm">
                                        <img src="{{ asset('template/img/profile.jpg') }}" alt="..." class="avatar-img rounded">
                                    </div>
                                </span>
                            </a>
                            <ul class="dropdown-menu dropdown-user animated fadeIn">
                                <div class="dropdown-user-scroll scrollbar-outer">
                                    <li>
                                        <div class="user-box">
                                            @if(Auth::user() != null)
                                                <input type="hidden" id="sessionId" value="{{ Auth::user()->id }}"/>
                                                <input type="hidden" id="rolId" value="{{ Auth::user()->rol_id }}"/>
                                                <input type="hidden" id="clienteId" value="{{ Auth::user()->cliente_id }}"/>
                                                <div class="avatar-lg"><img src="{{ asset('template/img/profile.jpg') }}" alt="image profile" class="avatar-img rounded"></div>
                                                <div class="u-text"  style="margin-top: 10px;">
                                                    <h4>{{ Auth::user()->nombre }}</h4>
                                                    <p class="text-muted">{{ Auth::user()->correo }}</p>
                                                    {{-- <a href="#" class="btn btn-xs btn-secondary btn-sm"  data-toggle="modal" data-target="#modalPerfil">Ver Perfil</a> --}}
                                                </div>
                                            @endif
                                        </div>
                                    </li>
                                    <li>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#"></a>
                                        <a class="dropdown-item"data-toggle="modal" data-target="#modalPerfil">Perfil</a>
                                        <a class="dropdown-item" href="#">Cambiar contraseña</a>
                                        <a class="dropdown-item" href="#">Configuración</a>
                                        <form action="{{ route('logout') }}" method="post">
                                            @csrf
                                            <button class="dropdown-item">Salir </button>
                                        </form>
                                    </li>
                                </div>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
            <!-- End Navbar -->
        </div>
    </div>
</div>



{{-- MOdal de Perfil --}}
<div class="modal fade" id="modalPerfil" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Mi Perfil</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="form-group" style="text-align: center">
                    <div class="avatar avatar-xxl">
                        <img src="{{ asset('template/img/profile.jpg') }}"  class="avatar-img rounded-circle">
                    </div>
                </div>
                <div class="form-group grupPerfil">
                    <i class="icon-user" style="padding-right: 10px;font-size: 23px;"></i>
                    <label for="txtArea" class="datosPerfil">  {{ Auth::user()->nombre." ".Auth::user()->apellido }} </label>
                </div>

                <div class="form-group grupPerfil">
                    <i class="icon-phone" style="padding-right: 10px;font-size: 23px;"></i>
                    <label for="txtArea" class="datosPerfil"> {{ Auth::user()->telefono }} </label>
                </div>

                <div class="form-group grupPerfil">
                    <i class="icon-envelope" style="padding-right: 15px;font-size: 23px;"></i>
                    <label for="txtArea" class="datosPerfil">  {{ Auth::user()->correo }} </label>
                </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" id="btnCerrarPerfil" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        </div>
        </div>
    </div>
</div>


{{-- Menu responsivo para el movil --}}
<div class="quick-sidebar">
    <a href="#" class="close-quick-sidebar">
        <i class="flaticon-cross"></i>
    </a>
    <div class="quick-sidebar-wrapper">
        <ul class="nav nav-tabs nav-line nav-color-secondary" role="tablist">
            <li class="nav-item"> <a class="nav-link active show" data-toggle="tab" href="#messages" role="tab" aria-selected="true">Messages</a> </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#tasks" role="tab" aria-selected="false">Tasks</a> </li>
            <li class="nav-item"> <a class="nav-link" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Settings</a> </li>
        </ul>
        <div class="tab-content mt-3">
            <div class="tab-chat tab-pane fade show active" id="messages" role="tabpanel">
                <div class="messages-contact">
                    <div class="quick-wrapper">
                        <div class="quick-scroll scrollbar-outer">
                            <div class="quick-content contact-content">
                                <span class="category-title mt-0">Contacts</span>
                                <div class="avatar-group">
                                    <div class="avatar">
                                        <img src="{{ asset('template/img/jm_denis.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                    </div>
                                    <div class="avatar">
                                        <img src="{{ asset('template/img/chadengle.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                    </div>
                                    <div class="avatar">
                                        <img src="{{ asset('template/img/mlane.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                    </div>
                                    <div class="avatar">
                                        <img src="{{ asset('template/img/talha.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                    </div>
                                    <div class="avatar">
                                        <span class="avatar-title rounded-circle border border-white">+</span>
                                    </div>
                                </div>
                                <span class="category-title">Recent</span>
                                <div class="contact-list contact-list-recent">
                                    <div class="user">
                                        <a href="#">
                                            <div class="avatar avatar-online">
                                                <img src="{{ asset('template/img/jm_denis.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                            </div>
                                            <div class="user-data">
                                                <span class="name">Jimmy Denis</span>
                                                <span class="message">How are you ?</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="user">
                                        <a href="#">
                                            <div class="avatar avatar-offline">
                                                <img src="{{ asset('template/img/chadengle.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                            </div>
                                            <div class="user-data">
                                                <span class="name">Chad</span>
                                                <span class="message">Ok, Thanks !</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="user">
                                        <a href="#">
                                            <div class="avatar avatar-offline">
                                                <img src="{{ asset('template/img/mlane.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                            </div>
                                            <div class="user-data">
                                                <span class="name">John Doe</span>
                                                <span class="message">Ready for the meeting today with...</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <span class="category-title">Other Contacts</span>
                                <div class="contact-list">
                                    <div class="user">
                                        <a href="#">
                                            <div class="avatar avatar-online">
                                                <img src="{{ asset('template/img/jm_denis.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                            </div>
                                            <div class="user-data2">
                                                <span class="name">Jimmy Denis</span>
                                                <span class="status">Online</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="user">
                                        <a href="#">
                                            <div class="avatar avatar-offline">
                                                <img src="{{ asset('template/img/chadengle.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                            </div>
                                            <div class="user-data2">
                                                <span class="name">Chad</span>
                                                <span class="status">Active 2h ago</span>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="user">
                                        <a href="#">
                                            <div class="avatar avatar-away">
                                                <img src="{{ asset('template/img/talha.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                            </div>
                                            <div class="user-data2">
                                                <span class="name">Talha</span>
                                                <span class="status">Away</span>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="messages-wrapper">
                    <div class="messages-title">
                        <div class="user">
                            <div class="avatar avatar-offline float-right ml-2">
                                <img src="{{ asset('template/img/chadengle.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                            </div>
                            <span class="name">Chad</span>
                            <span class="last-active">Active 2h ago</span>
                        </div>
                        <button class="return">
                            <i class="flaticon-left-arrow-3"></i>
                        </button>
                    </div>
                    <div class="messages-body messages-scroll scrollbar-outer">
                        <div class="message-content-wrapper">
                            <div class="message message-in">
                                <div class="avatar avatar-sm">
                                    <img src="{{ asset('template/img/chadengle.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                </div>
                                <div class="message-body">
                                    <div class="message-content">
                                        <div class="name">Chad</div>
                                        <div class="content">Hello, Rian</div>
                                    </div>
                                    <div class="date">12.31</div>
                                </div>
                            </div>
                        </div>
                        <div class="message-content-wrapper">
                            <div class="message message-out">
                                <div class="message-body">
                                    <div class="message-content">
                                        <div class="content">
                                            Hello, Chad
                                        </div>
                                    </div>
                                    <div class="message-content">
                                        <div class="content">
                                            What's up?
                                        </div>
                                    </div>
                                    <div class="date">12.35</div>
                                </div>
                            </div>
                        </div>
                        <div class="message-content-wrapper">
                            <div class="message message-in">
                                <div class="avatar avatar-sm">
                                    <img src="{{ asset('template/img/chadengle.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                </div>
                                <div class="message-body">
                                    <div class="message-content">
                                        <div class="name">Chad</div>
                                        <div class="content">
                                            Thanks
                                        </div>
                                    </div>
                                    <div class="message-content">
                                        <div class="content">
                                            When is the deadline of the project we are working on ?
                                        </div>
                                    </div>
                                    <div class="date">13.00</div>
                                </div>
                            </div>
                        </div>
                        <div class="message-content-wrapper">
                            <div class="message message-out">
                                <div class="message-body">
                                    <div class="message-content">
                                        <div class="content">
                                            The deadline is about 2 months away
                                        </div>
                                    </div>
                                    <div class="date">13.10</div>
                                </div>
                            </div>
                        </div>
                        <div class="message-content-wrapper">
                            <div class="message message-in">
                                <div class="avatar avatar-sm">
                                    <img src="{{ asset('template/img/chadengle.jpg') }}" alt="..." class="avatar-img rounded-circle border border-white">
                                </div>
                                <div class="message-body">
                                    <div class="message-content">
                                        <div class="name">Chad</div>
                                        <div class="content">
                                            Ok, Thanks !
                                        </div>
                                    </div>
                                    <div class="date">13.15</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="messages-form">
                        <div class="messages-form-control">
                            <input type="text" placeholder="Type here" class="form-control input-pill input-solid message-input">
                        </div>
                        <div class="messages-form-tool">
                            <a href="#" class="attachment">
                                <i class="flaticon-file"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="tasks" role="tabpanel">
                <div class="quick-wrapper tasks-wrapper">
                    <div class="tasks-scroll scrollbar-outer">
                        <div class="tasks-content">
                            <span class="category-title mt-0">Today</span>
                            <ul class="tasks-list">
                                <li>
                                    <label class="custom-checkbox custom-control checkbox-secondary">
                                        <input type="checkbox" checked="" class="custom-control-input"><span class="custom-control-label">Planning new project structure</span>
                                        <span class="task-action">
                                            <a href="#" class="link text-danger">
                                                <i class="flaticon-interface-5"></i>
                                            </a>
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-checkbox custom-control checkbox-secondary">
                                        <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Create the main structure							</span>
                                        <span class="task-action">
                                            <a href="#" class="link text-danger">
                                                <i class="flaticon-interface-5"></i>
                                            </a>
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-checkbox custom-control checkbox-secondary">
                                        <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Add new Post</span>
                                        <span class="task-action">
                                            <a href="#" class="link text-danger">
                                                <i class="flaticon-interface-5"></i>
                                            </a>
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-checkbox custom-control checkbox-secondary">
                                        <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Finalise the design proposal</span>
                                        <span class="task-action">
                                            <a href="#" class="link text-danger">
                                                <i class="flaticon-interface-5"></i>
                                            </a>
                                        </span>
                                    </label>
                                </li>
                            </ul>

                            <span class="category-title">Tomorrow</span>
                            <ul class="tasks-list">
                                <li>
                                    <label class="custom-checkbox custom-control checkbox-secondary">
                                        <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Initialize the project							</span>
                                        <span class="task-action">
                                            <a href="#" class="link text-danger">
                                                <i class="flaticon-interface-5"></i>
                                            </a>
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-checkbox custom-control checkbox-secondary">
                                        <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Create the main structure							</span>
                                        <span class="task-action">
                                            <a href="#" class="link text-danger">
                                                <i class="flaticon-interface-5"></i>
                                            </a>
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-checkbox custom-control checkbox-secondary">
                                        <input type="checkbox" class="custom-control-input"><span class="custom-control-label">Updates changes to GitHub							</span>
                                        <span class="task-action">
                                            <a href="#" class="link text-danger">
                                                <i class="flaticon-interface-5"></i>
                                            </a>
                                        </span>
                                    </label>
                                </li>
                                <li>
                                    <label class="custom-checkbox custom-control checkbox-secondary">
                                        <input type="checkbox" class="custom-control-input"><span title="This task is too long to be displayed in a normal space!" class="custom-control-label">This task is too long to be displayed in a normal space!				</span>
                                        <span class="task-action">
                                            <a href="#" class="link text-danger">
                                                <i class="flaticon-interface-5"></i>
                                            </a>
                                        </span>
                                    </label>
                                </li>
                            </ul>

                            <div class="mt-3">
                                <div class="btn btn-primary btn-rounded btn-sm">
                                    <span class="btn-label">
                                        <i class="fa fa-plus"></i>
                                    </span>
                                    Add Task
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="settings" role="tabpanel">
                <div class="quick-wrapper settings-wrapper">
                    <div class="quick-scroll scrollbar-outer">
                        <div class="quick-content settings-content">

                            <span class="category-title mt-0">General Settings</span>
                            <ul class="settings-list">
                                <li>
                                    <span class="item-label">Enable Notifications</span>
                                    <div class="item-control">
                                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
                                    </div>
                                </li>
                                <li>
                                    <span class="item-label">Signin with social media</span>
                                    <div class="item-control">
                                        <input type="checkbox" data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
                                    </div>
                                </li>
                                <li>
                                    <span class="item-label">Backup storage</span>
                                    <div class="item-control">
                                        <input type="checkbox" data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
                                    </div>
                                </li>
                                <li>
                                    <span class="item-label">SMS Alert</span>
                                    <div class="item-control">
                                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
                                    </div>
                                </li>
                            </ul>

                            <span class="category-title mt-0">Notifications</span>
                            <ul class="settings-list">
                                <li>
                                    <span class="item-label">Email Notifications</span>
                                    <div class="item-control">
                                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
                                    </div>
                                </li>
                                <li>
                                    <span class="item-label">New Comments</span>
                                    <div class="item-control">
                                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
                                    </div>
                                </li>
                                <li>
                                    <span class="item-label">Chat Messages</span>
                                    <div class="item-control">
                                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
                                    </div>
                                </li>
                                <li>
                                    <span class="item-label">Project Updates</span>
                                    <div class="item-control">
                                        <input type="checkbox" data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
                                    </div>
                                </li>
                                <li>
                                    <span class="item-label">New Tasks</span>
                                    <div class="item-control">
                                        <input type="checkbox" checked data-toggle="toggle" data-onstyle="primary" data-style="btn-round">
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="sidebar sidebar-style-2" data-background-color="dark2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="{{ asset('template/img/profile.jpg') }}" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a data-toggle="collapse" href="#collapseExample" aria-expanded="true">
                        <span>
                            Hizrian
                            <span class="user-level">Administrator</span>
                            <span class="caret"></span>
                        </span>
                    </a>
                    <div class="clearfix"></div>

                    <div class="collapse in" id="collapseExample">
                        <ul class="nav">
                            <li>
                                <a href="#profile">
                                    <span class="link-collapse">My Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#edit">
                                    <span class="link-collapse">Edit Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="#settings">
                                    <span class="link-collapse">Settings</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <ul class="nav nav-primary" >
                <li @if(Request::path() == 'inmuebles'){!! "class='nav-item active'" !!} @else {!! "class='nav-item'" !!} @endif >
                    <a href="{{ route('inmuebles') }}">
                        <i class="fas fa-table"></i> <p>Inmuebles</p> 
                    </a>
                </li>

                <li @if(Request::path() == 'usuarios'){!! "class='nav-item active'" !!} @else {!! "class='nav-item'" !!} @endif >
                    <a href="{{ route('usuarios') }}">
                        <i class="fas fa-users"></i> <p>Usuarios</p> 
                    </a>
                </li>

                <li @if(Request::path() == 'paquetes'){!! "class='nav-item active'" !!} @else {!! "class='nav-item'" !!} @endif >
                    <a href="{{ route('paquetes') }}">
                        <i class="fas fa-award"></i> <p>Paquetes</p> 
                    </a>
                </li>

                <li @if(str_contains(Request::path(),'catalogos/') == 1)  
                            {!!"class='nav-item submenu active'"!!} 
                        @else 
                             {!!"class='nav-item'"!!} 
                        @endif >

                    <a data-toggle="collapse" href="#menuCatalogos" class="collapsed" aria-expanded="false" >
                        <i class="fas fa-layer-group"></i> <p>Catalogos</p><span class="caret"></span>
                    </a>
                    
                    <div 
                        @if(str_contains(Request::path(),'catalogos/') == 1)  
                            {!!"class='collapse show'"!!} 
                        @else 
                             {!!"class='collapse'"!!} 
                        @endif
                         id="menuCatalogos">

                        <ul class="nav nav-collapse">
                            <li @if (Request::path() == 'catalogos/orientaciones') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('cat-orientaciones') }}">
                                    <span class="sub-item">Orientaciones</span>
                                </a>
                            </li>
                            <li @if (Request::path() == 'catalogos/unidadDeMedida') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('cat-unidad-de-medidas') }}">
                                    <span class="sub-item">Unidades de Medida</span>
                                </a>
                            </li>
                            <li @if (Request::path() == 'catalogos/areas') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('cat-areas') }}">
                                    <span class="sub-item">Areas</span>
                                </a>
                            </li>
                            <li @if (Request::path() == 'catalogos/tipos') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('cat-tipos') }}">
                                    <span class="sub-item">Tipos</span>
                                </a>
                            </li>
                            <li @if (Request::path() == 'catalogos/niveles') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('cat-niveles') }}">
                                    <span class="sub-item">Niveles</span>
                                </a>
                            </li>
                            <li @if (Request::path() == 'catalogos/usos') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('cat-usos') }}">
                                    <span class="sub-item">Usos</span>
                                </a>
                            </li>
                            
                            <li @if (Request::path() == 'catalogos/lineas') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('cat-tipo-lineas') }}">
                                    <span class="sub-item">Tipo Linea</span>
                                </a>
                            </li>

                            <li @if (Request::path() == 'catalogos/clasificaciones') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('cat-clasificaciones') }}">
                                    <span class="sub-item">Clasificaciones</span>
                                </a>
                            </li>

                            <li @if (Request::path() == 'catalogos/colindancias') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('cat-colindancia') }}">
                                    <span class="sub-item">Colindancias</span>
                                </a>
                            </li>
                            <li @if (Request::path() == 'catalogos/subscripciones') {!! 'class="active"' !!} @endif>
                                <a href="{{ route('subscripcion') }}">
                                    <span class="sub-item">Subscripciones</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-section">
                    <span class="sidebar-mini-icon">
                        <i class="fa fa-ellipsis-h"></i>
                    </span>
                    <h4 class="text-section">Components</h4>
                </li>
                <li class="nav-item">
                    <a data-toggle="collapse" href="#base">
                        <i class="fas fa-layer-group"></i>
                        <p>Base</p>
                        <span class="caret"></span>
                    </a>
                    <div class="collapse" id="base">
                        <ul class="nav nav-collapse">
                            <li>
                                <a href="components/avatars.html">
                                    <span class="sub-item">Avatars</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/buttons.html">
                                    <span class="sub-item">Buttons</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/gridsystem.html">
                                    <span class="sub-item">Grid System</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/panels.html">
                                    <span class="sub-item">Panels</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/notifications.html">
                                    <span class="sub-item">Notifications</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/sweetalert.html">
                                    <span class="sub-item">Sweet Alert</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/lists.html">
                                    <span class="sub-item">Lists</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/owl-carousel.html">
                                    <span class="sub-item">Owl Carousel</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/magnific-popup.html">
                                    <span class="sub-item">Magnific Popup</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/font-awesome-icons.html">
                                    <span class="sub-item">Font Awesome Icons</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/simple-line-icons.html">
                                    <span class="sub-item">Simple Line Icons</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/flaticons.html">
                                    <span class="sub-item">Flaticons</span>
                                </a>
                            </li>
                            <li>
                                <a href="components/typography.html">
                                    <span class="sub-item">Typography</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>           
        </div>
    </div>
</div>
@section('title', 'Areas')

@section('content')
   <input type="hidden" value="{{ $inmueble->nombre }}" id="tituloInmueble"/>
   <input type="hidden" value="{{ $inmueble->id }}" id="idInmueble"/>
   <input type="hidden" value="{{ $accion }}" id="idAccion"/>
   
   @if ($accion != 'add')
      <input type="hidden" value="{{ $area->id }}" id="idArea"/>
   @endif
  
   <div style="display:none;">
      <form action="/areas/{{ $inmueble->id }}" method="get">
         @csrf
         <input type="submit" id="salirArea"></input>
      </form>
  </div>

   

   @if ($accion == 'add' || $accion == 'update')
      <particulares />
   @else
      <areas-create />
   @endif

@endsection

@extends('layout.Main')
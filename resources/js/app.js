/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue').default;

/* import VeeValidate from 'vee-validate';
Vue.use(VeeValidate); */

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('catalogo-orientaciones', require('./components/Catalogos/OrientacionComponent.vue').default);
Vue.component('catalogo-unidad-medida', require('./components/Catalogos/UnidaDeMedidaComponent.vue').default);
Vue.component('catalogo-areas', require('./components/Catalogos/AreaComponent.vue').default);
Vue.component('catalogo-tipos', require('./components/Catalogos/TipoComponent.vue').default);
Vue.component('catalogo-niveles', require('./components/Catalogos/NivelComponent.vue').default);
Vue.component('catalogo-usos', require('./components/Catalogos/UsoComponent.vue').default);
Vue.component('catalogo-tipo-linea', require('./components/Catalogos/TipoLineaComponent.vue').default);
Vue.component('cat-colindancias', require('./components/Catalogos/ColindanciaComponent.vue').default);
Vue.component('cat-clasificaciones', require('./components/Catalogos/ClasificacionComponent.vue').default);
Vue.component('subscripcion', require('./components/Catalogos/SubscripcionComponent.vue').default);

Vue.component('usuarios', require('./components/UsuarioComponent.vue').default);
Vue.component('inmuebles', require('./components/Configuraciones/InmuebleComponent.vue').default);



Vue.component('areas', require('./components/Configuraciones/Area/Index.vue').default);
Vue.component('areas-create', require('./components/Configuraciones/Area/Create.vue').default);
Vue.component('particulares', require('./components/Configuraciones/Area/Particulares.vue').default);


Vue.component('paquetes', require('./components/PaquetesComponent.vue').default);
Vue.component('Comprarpaquete', require('./components/Subscripciones/ComprarpaqueteComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app',
});

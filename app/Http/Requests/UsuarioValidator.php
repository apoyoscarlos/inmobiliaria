<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class UsuarioValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'rol_id'    => 'required|integer|exists:roles,id',
            'nombre'    => 'required|string|max:200',
            'apellido'  => 'required|string|max:200',
            'celular'   => 'nullable|string|max:20',
            'correo'    => 'required|string|email|unique:usuarios|max:200',
            'password'  => 'required|string|max:200',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'        => 'required|integer|exists:usuarios,id',
            'rol_id'    => 'required|integer|exists:roles,id',
            'nombre'    => 'required|string|max:200',
            'apellido'  => 'required|string|max:200',
            'celular'   => 'nullable|string|max:20',
            'correo'    => ['required','string','max:200',Rule::unique('usuarios')->ignore($request['id']),],
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id'        => 'required|integer|exists:usuarios,id',
            'accion'    => ['required','string', Rule::in(['delete', 'status'])],
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function password(Request $request){
        $validator = Validator::make($request->all(), [
            'id'        => 'required|integer|exists:usuarios,id',
            'password'  => 'required|string|max:200',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
    
    public function find(Request $request){
        $validator = Validator::make($request->all(), [
            'order'     => ['nullable','string', Rule::in(['asc', 'desc'])],
            'order_by'  => ['nullable','string', Rule::in(['id', 'cliente','nombre','apellido', 'correo', 'telefono', 'rol', 'activo' ])],
            'show'      => 'nullable|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
}

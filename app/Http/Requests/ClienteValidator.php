<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class ClienteValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function find(Request $request){
        $validator = Validator::make($request->all(), [
            'order'     => ['nullable','string', Rule::in(['asc', 'desc'])],
            'order_by'  => ['nullable','string', Rule::in(['id', 'nombre', 'correo', 'activo'])],
            'show'      => 'nullable|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
}

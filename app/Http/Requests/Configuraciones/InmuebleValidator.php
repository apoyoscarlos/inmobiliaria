<?php

namespace App\Http\Requests\Configuraciones;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class InmuebleValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'cliente_id'    => 'required|integer|exists:clientes,id',
            'nombre'        => 'required|string|max:200',
            'municipio'     => 'required|string|max:200',
            'colonia'       => 'required|string|max:200',
            'calle'         => 'required|string|max:200',
            'numero'        => 'required|string|max:200',
            'descripcion'   => 'nullable|string',
            'terreno_exclusivo'     => 'nullable|numeric',
            'terreno_comun'         => 'nullable|numeric',
            'constuccion_exclusiva' => 'nullable|numeric',
            'constuccion_comun'     => 'nullable|numeric'
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'            => 'required|integer|exists:inmuebles,id',
            'cliente_id'    => 'required|integer|exists:clientes,id',
            'nombre'        => 'required|string|max:200',
            'municipio'     => 'required|string|max:200',
            'colonia'       => 'required|string|max:200',
            'calle'         => 'required|string|max:200',
            'numero'        => 'required|string|max:200',
            'descripcion'   => 'nullable|string',
            'terreno_exclusivo'     => 'nullable|numeric',
            'terreno_comun'         => 'nullable|numeric',
            'constuccion_exclusiva' => 'nullable|numeric',
            'constuccion_comun'     => 'nullable|numeric'
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function find(Request $request){
        $validator = Validator::make($request->all(), [
            'order'     => ['nullable','string', Rule::in(['asc', 'desc'])],
            'order_by'  => ['nullable','string', Rule::in(['id', 'nombre', 'direccion', 'descripcion', 'cliente', 'terreno_exclusivo', 'terreno_comun', 'constuccion_comun', 'constuccion_exclusiva' ])],
            'show'      => 'nullable|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function generacion(Request $request){
        $validator = Validator::make($request->all(), [
            'id'   => 'required|integer|exists:inmuebles,id',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
}

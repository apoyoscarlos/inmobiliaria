<?php

namespace App\Http\Requests\Configuraciones;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class AreaValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'tipo'          => ['nullable','string', Rule::in(['general', 'particular'])],
            'inmueble_id'   => 'required|integer|exists:inmuebles,id',
            'comentarios'   => 'nullable|string',
            'cat_tipo_id'   => 'required|integer|exists:cat-tipos,id',
            'no'            => 'required|string|max:50',
            'comentarios'   => 'nullable|string',
            'construccion_exclusiva'=> 'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'unidad_medida_ce_id'   => 'nullable|integer|exists:cat-unidad_medidas,id',
        ]);

        if ($validator->fails())
            return response()->json($validator->errors());
        

        if($request->tipo  == "general" ){
            $validator = Validator::make($request->all(), [
                'terreno_exclusivo'     => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'terreno_comun'         => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'construccion_comun'    => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'clase'                 => 'nullable|string|max:250',
                'cuota_participacion'   => 'required|regex:/^\d+(\.\d{1,9})?$/'    
            ]);
        }
        else{
            $validator = Validator::make($request->all(), [
                'cat_area_id'   => 'nullable|integer|exists:cat-areas,id',
                'cat_nivel_id'  => 'required|integer|exists:cat-niveles,id',
                'cat_uso_id'    => 'required|integer|exists:cat-usos,id',
                'cat_clasificacion_id'      => 'nullable|integer|exists:cat-clasificaciones,id',
                'cat_clasificacion_dos_id'  => 'nullable|integer|exists:cat-clasificaciones,id',
            ]);
        }

        if ($validator->fails())
            return response()->json($validator->errors());
        
        return true;
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'            => 'required|integer|exists:areas,id',
            'tipo'          => ['nullable','string', Rule::in(['general', 'particular'])],
            'inmueble_id'   => 'required|integer|exists:inmuebles,id',
            'comentarios'   => 'nullable|string',
            'no'            => 'required|string|max:50',
            'comentarios'   => 'nullable|string',
            'construccion_exclusiva'=> 'nullable|regex:/^\d+(\.\d{1,2})?$/',
            'unidad_medida_ce_id'   => 'nullable|integer|exists:cat-unidad_medidas,id',
        ]);
       
        if ($validator->fails())
            return response()->json($validator->errors());
        

        if($request->tipo  == "general" ){
            $validator = Validator::make($request->all(), [
                'terreno_exclusivo'     => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'terreno_comun'         => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'construccion_comun'    => 'required|regex:/^\d+(\.\d{1,2})?$/',
                'clase'                 => 'nullable|string|max:250',
                'cuota_participacion'   => 'required|regex:/^\d+(\.\d{1,9})?$/'    
            ]);
        }
        else{
            $validator = Validator::make($request->all(), [
                'cat_area_id'   => 'nullable|integer|exists:cat-areas,id',
                'cat_tipo_id'   => 'required|integer|exists:cat-tipos,id',
                'cat_nivel_id'  => 'required|integer|exists:cat-niveles,id',
                'cat_uso_id'    => 'required|integer|exists:cat-usos,id',
                'cat_clasificacion_id'      => 'nullable|integer|exists:cat-clasificaciones,id',
                'cat_clasificacion_dos_id'  => 'nullable|integer|exists:cat-clasificaciones,id',
            ]);
        }

        if ($validator->fails())
            return response()->json($validator->errors());
        
        return true;
    }

    public function find(Request $request){
        $validator = Validator::make($request->all(), [
            'order'     => ['nullable','string', Rule::in(['asc', 'desc'])],
            'order_by'  => ['nullable','string', Rule::in(['id', 'codigo','area', 'tipo', 'clase', 'inmueble' ])],
            'show'      => 'nullable|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function codigo(Request $request){
        $validator = Validator::make($request->all(), [
            'inmueble_id'   => 'required|integer|exists:inmuebles,id',
            'no'            => 'required|string',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function getParticulares(Request $request){
        $validator = Validator::make($request->all(), [
            'id'   => 'required|integer|exists:areas,id',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
    
    public function saveParticulares(Request $request){
        $validator = Validator::make($request->all(), [
            'tipo'          => 'required|string',
            'array'         => 'required|array',
            'eliminar'      => 'nullable|array',
            'general_id'    => 'nullable|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
}

<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class GeneralValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function exportar(Request $request){
        $validator = Validator::make($request->all(), [
            'cliente_id'=> 'nullable|integer|exists:clientes,id',
            'tipo'      => ['required','string', 
                Rule::in([
                    'cat-areas',
                    'cat-niveles',
                    'cat-orientaciones',
                    'cat-tipo-lineas', 'cat-tipos',
                    'cat-unidad-medidas',
                    'cat-usos',
                    'cat-colindancia',
                    'cat-clasificacion'
                ])],
            'extension' => ['required','string', Rule::in(['xlsx', 'csv', 'pdf'])],
            'paginate'  => ['required', Rule::in([false])],
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function importar(Request $request){
        $validator = Validator::make($request->all(), [
            'cliente_id'=> 'required|integer|exists:clientes,id',
            'file'      => 'required|max:5000|mimes:xls,xlsx,csv',
            'tipo'      => ['required','string', 
                Rule::in([
                    'cat-areas',
                    'cat-niveles',
                    'cat-orientaciones',
                    'cat-tipo-lineas', 'cat-tipos',
                    'cat-unidad-medidas',
                    'cat-usos',
                    'cat-colindancia',
                    'cat-clasificacion'
                ])],
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function conversor(Request $request){
        $validator = Validator::make($request->all(), [
            'numero'    => 'required|numeric',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
}

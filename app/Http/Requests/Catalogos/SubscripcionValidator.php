<?php

namespace App\Http\Requests\Catalogos;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Http\FormRequest;

class SubscripcionValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }

    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'cliente_id'    => 'required|integer|exists:clientes,id',      
            'nombre'        => 'required|string|max:200',
            'descripcion'   => 'required|string',
            //'gratuito'   => 'required|boolean',
            'costo'   => 'required|numeric',
            'creditos'   => 'required|integer',
            'activo'        => 'nullable|boolean',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function update(Request $request){
        $validator = Validator::make($request->all(), [
            'id'            => 'required|integer|exists:subscripciones,id',
            'cliente_id'    => 'required|integer|exists:clientes,id',      
            'nombre'        => 'required|string|max:200',
            'descripcion'   => 'required|string',
            'gratuito'   => 'required|boolean',
            'costo'   => 'required|numeric',
            'creditos'   => 'required|integer',
            'activo'        => 'nullable|boolean',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function delete(Request $request){
        $validator = Validator::make($request->all(), [
            'id'        => 'required|integer|exists:subscripciones,id',
            'accion'    => ['required','string', Rule::in(['delete', 'status'])],
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }

    public function find(Request $request){
        $validator = Validator::make($request->all(), [
            'order'     => ['nullable','string', Rule::in(['asc', 'desc'])],
            'order_by'  => ['nullable','string', Rule::in(['id','nombre', 'descripcion', 'activo', 'cliente' ])],
            'show'      => 'nullable|integer',
        ]);
       
        if ($validator->fails()) {
            return response()->json($validator->errors());
        }
        return true;
    }
}

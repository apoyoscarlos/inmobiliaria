<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UsuarioValidator;

class UsuarioController extends Controller
{
    protected $validate;
    protected $buscar;
    public function __construct() {
        $this->validate = new UsuarioValidator();
    }

    /**
     * Crea un registro en la db
     * @param Request   $request
     * @return User
     */
    public function store(Request $request){
        $validation = $this->validate->store($request);
        $data = $request->all();

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
        $data['correo']     = strtolower($data['correo']);
        $data['password']   = bcrypt($request->password);
        $cliente            = Cliente::create($data);
        $data['cliente_id'] = $cliente->id;
        $item               = User::create($data);
        
        return response()->json(['id'=> $item->id], 200);
    }

    /**
     * Retorna un Objeto tipo Usuario, si este existe en la db
     * @param Integer   $id
     * @return Object
     */
    public function get($id){
        $item = User::find($id);
        if(is_null($item))
            return response()->json( ['error'=> "No se encontro el registro con id ".$id], 403);
       
        return response()->json($item, 200);
    }

    /**
     * Actualiza un registro en la db
     * @param Request   $request
     * @return Boolean
     */
    public function update(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->update($request);
            $data = $request->all();

            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);
            if(isset($request->password))
                unset($data['password']);

            $data['correo']     = strtolower($data['correo']);
            User::where('id', $request->id)->update($data);
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Elimina o deshabilita un registro en la db
     * @param Request   $request
     * @return Object
     */
    public function delete(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->delete($request);
            $data = $request->all();

            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);

            $item = User::find($request->id);
            if($request->accion == 'delete')
                $item->delete();
            else{
                $item->activo = !$item->activo;
                $item->save();
            }
            
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Cambia el password del usuario
     * @param Request   $request
     * @return Object
     */
    public function password(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->password($request);
            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);

            $item = User::find($request->id);
            $item->password = bcrypt($request->password);
            $item->save();

            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Retorna un array de objeto tipo usuario
     * @param Request   $request
     * @return Array
     */
    public function find(Request $request){
        $items      = [];
        $show       = 10;
        $order      = 'desc';
        $order_by   = 'u.id';

        $validation = $this->validate->find($request);
        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);

        $query = DB::table('usuarios as u')
                    ->leftJoin('clientes as c', 'u.cliente_id', '=', 'c.id')
                    ->leftJoin('roles as r', 'u.rol_id', '=', 'r.id')
                    ->select('u.*', 'r.nombre as rol', 'c.nombre as cli_nombre', 'c.apellido as cli_apellido');

        if(!is_null($request->nombre)){
            $this->buscar = $request->nombre;
            $query->orWhere(function ($query) {
                $query->where('u.nombre','ilike', '%'.$this->buscar.'%')
                    ->orWhere("u.apellido",'ilike', '%'.$this->buscar.'%');
            });
        }

        if(!is_null($request->apellido))
            $query->where('u.apellido','ilike', '%'.$request->apellido.'%');

        if(!is_null($request->correo))
            $query->where('u.correo','ilike', '%'.$request->correo.'%');
        
        if(!is_null($request->cliente_id))
            $query->where('u.cliente_id', $request->cliente_id);

        if(!is_null($request->rol_id))
            $query->where('u.rol_id', $request->rol_id);

        if(!is_null($request->activo)){
            $activo =  $request->activo == 'true' ? true : false;
            $query->where('u.activo', $activo);
        }
        
        /* Parametros para la paginanacion y el orden */
        if(!is_null($request->order))
            $order = $request->order;

        if(!is_null($request->order_by))
            $order_by = $request->order_by;
        
        if(!is_null($request->order_by)){
            $order_by = 'u.'.$request->order_by;
            
            if($request->order_by == 'cliente')
                $order_by = 'c.nombre';
            elseif($request->order_by == 'rol')
                $order_by = 'r.nombre';
        }
        $query->orderBy($order_by, $order); 
       
        if(is_null($request->paginate) || $request->paginate == "true" ){
            if(!is_null($request->show))
                $show = $request->show;
            
            $items = $query->paginate($show);
        }
        else{
            $items = $query->get();            
            return response()->json(["data" => $items], 200);
        }
        
        return response()->json($items, 200);
    }
}

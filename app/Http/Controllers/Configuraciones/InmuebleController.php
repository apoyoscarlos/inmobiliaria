<?php

namespace App\Http\Controllers\Configuraciones;

use Exception;
use Illuminate\Http\Request;
use App\Library\ConversorNumerico;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Configuraciones\Area;
use App\Models\Configuraciones\Inmueble;
use App\Models\Configuraciones\Colindancia;
use App\Http\Requests\Configuraciones\InmuebleValidator;

class InmuebleController extends Controller
{
    protected $validate;
    public function __construct() {
        $this->validate = new InmuebleValidator();
    }

    /**
     * Crea un registro en la db
     * @param Request   $request
     * @return Integer
     */
    public function store(Request $request){
        $validation = $this->validate->store($request);
        $data       = $request->all();

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
        $item = Inmueble::create($data);
        return response()->json(['id'=> $item->id], 200);
    }

    /**
     * Retorna un Objeto tipo Inmueble, si este existe en la db
     * @param Integer   $id
     * @return Object
     */
    public function get($id){
        $inmueble = Inmueble::with('areas')->where('id', $id)->get();
        if(sizeof($inmueble) == 0)
            return response()->json( ['error'=> "No se encontro el registro con id ".$id], 403);
       
        return response()->json($inmueble, 200);
    }

    /**
     * Actualiza un registro en la db
     * @param Request   $request
     * @return Boolean
     */
    public function update(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->update($request);
            $data = $request->all();

            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);
            
            Inmueble::where('id', $request->id)->update($data);
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Elimina un registro en la db
     * @param Request   $request
     * @return Object
     */
    public function delete(Request $request){
        $item = Inmueble::find($request->id);
        if(is_null($item))
            return response()->json( ['error'=> "No se encontro el registro con id ".$request->id], 403);
        
        $item->delete();
        return response()->json(['message'=> "Operacion exitosa."], 200);
    }

    /**
     * Retorna un array de objeto tipo Inmueble
     * @param Request   $request
     * @return Array
     */
    public function find(Request $request){
        $items      = [];
        $show       = 10;
        $order      = 'desc';
        $order_by   = 'i.id';

        $validation = $this->validate->find($request);
        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);

        $query = DB::table('inmuebles as i')
                    ->leftJoin('clientes as cl', 'cl.id', '=', 'i.cliente_id')
                    ->select('i.*', 'cl.nombre as cli_nombre', 'cl.apellido as cli_apellido');

        if(!is_null($request->cliente_id))
            $query->where('i.cliente_id', $request->cliente_id);

        if(!is_null($request->nombre))
            $query->where('i.nombre','ilike', '%'.$request->nombre.'%');

        if(!is_null($request->municipio))
            $query->where('i.municipio','ilike', '%'.$request->municipio.'%');

        if(!is_null($request->descripcion))
            $query->where('i.descripcion','ilike', '%'.$request->descripcion.'%');
        
        /* Parametros para la paginanacion y el orden */
        if(!is_null($request->order))
            $order = $request->order;

        if(!is_null($request->order_by)){
            $order_by = 'i.'.$request->order_by;
            
            if($request->order_by == 'cliente')
                $order_by = 'cl.nombre';
            if($request->order_by == 'direccion')
                $order_by = 'municipio';
        }

        $query->orderBy($order_by, $order); 
        if(is_null($request->paginate) || $request->paginate == "true" ){
            if(!is_null($request->show))
                $show = $request->show;
            
            $items = $query->paginate($show);
        }
        else{
            $items = $query->get();            
            return response()->json(["data" => $items], 200);
        }
        
        return response()->json($items, 200);
    }

    /**
     * Retorna un archivo docx con la descripcion del inmueble
     * @param Request   $request
     * @return DOCX
     */
    public function generacion(Request $request){
        $grupos     = array();
        $final      = array();
        $validation = $this->validate->generacion($request);

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);

        $areas      = Area::where('inmueble_id',$request->id)->select('no')->groupBY('no')->orderBy('no','asc')->get();
        $conversor  = new ConversorNumerico();
        foreach ($areas as $area) {
            $temp = Area::where(['inmueble_id' => $request->id, 'no' => $area->no])->orderBy('codigo','asc')->get();
            array_push($grupos, $temp);
        }

        foreach ($grupos as $i => $grupo) {
            $texto = "";

            if($i == 1  || $i == 2 ){
                foreach ($grupo as $area) {
                    $arrayNo    = explode("-",$area->no);
                    $textNo     = $conversor->convertir(intval($arrayNo[1]));
                    $cat_area   = strtolower($area->area->nombre) == 'privativa' ? " unidad de propiedad exclusiva perteneciente al condominio denominado “----”, condominio constituido en el predio " : "";
                    $ubicacion  = " ubicado en esta ciudad y municipio de Mérida, en la calle 38 (treinta y ocho) número 216 (Doscientos Dieciseis) de la Colonia ----- ";
                    $consExclu  = "con una superficie exclusiva total de ".$area->construccion_exclusiva." ".$area->unidad_medida_ce->nombre;
                    $textExclu  = $conversor->convertir($area->construccion_exclusiva)." ".$area->unidad_medida_ce->descripcion;
                    $perimetro  = " cuyo perimetro se describe a continuación: partiendo del vértice sureste con dirección al ";

                    $colindancias   = Colindancia::where('area_id', $area->id)->orderBy('id','asc')->get();
                    $textCol        = "";
                    foreach ($colindancias as $key => $col) {
                        $textCol    = $textCol.$col->orientacion->descripcion." en linea ".$col->linea->nombre." colindando con ".$col->colindancia->comentarios." mide ".$col->distancia_colindancia.$col->unidadDistancia->nombre;
                        $textDisCol = $conversor->convertir($col->distancia_colindancia);
                        $textCol    = $textCol." (".$textDisCol." ".$col->unidadDistancia->descripcion."), ";
                        
                        if($key < (sizeof($colindancias) - 2 ))
                            $textCol = $textCol."de este punto en dirección al ";
                        elseif($key < (sizeof($colindancias) - 1))
                            $textCol = $textCol."para cerrar el perimetro que se describe con dirección al ";
                        else
                            $textCol = substr($textCol,0,-2);
                    }

                    $textParticipacion  =   $conversor->convertir($area->cuota_participacion);
                    $textCol            =   $textCol.". Corresponde al ".
                                            $area->tipo->nombre." ".
                                            $area->no." (".$textNo."), cuota de participación del ".
                                            $area->cuota_participacion."% (".
                                            $textParticipacion." PORCIERTO).";
  
                    $texto =    $area->tipo->nombre." ".
                                $area->no." (".$textNo."),".
                                $cat_area.
                                $area->clase.
                                $ubicacion.
                                $consExclu." (".$textExclu."),".
                                $perimetro.
                                $textCol;

                    $depto = ["descripcion" => $texto];  
                    array_push($final, $depto);
                }
            }
        }



        return response()->json($grupos, 200);
    }
}

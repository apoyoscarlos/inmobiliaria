<?php

namespace App\Http\Controllers\Configuraciones;

use Exception;

use Illuminate\Http\Request;
use App\Models\Catalogos\Uso;
use App\Models\Catalogos\Tipo;
use App\Models\Catalogos\Nivel;
use Illuminate\Support\Facades\DB;
use App\Models\Catalogos\TipoLinea;
use App\Http\Controllers\Controller;
use App\Models\Configuraciones\Area;
use Illuminate\Support\Facades\Auth;
use App\Models\Catalogos\Colindancia;
use App\Models\Catalogos\Orientacion;
use App\Models\Catalogos\UnidadMedida;
use App\Models\Catalogos\Clasificacion;
use App\Models\Configuraciones\Inmueble;
use App\Models\Catalogos\Area as CatalogosArea;
use App\Http\Requests\Configuraciones\AreaValidator;
use App\Models\Configuraciones\Colindancia as ConfiguracionesColindancia;
use Ramsey\Uuid\Type\Integer;

class AreaControler extends Controller
{
    protected $validate;
    public function __construct() {
        $this->validate = new AreaValidator();
    }

    /**
     * Crea un registro en la db
     * @param Request   $request
     * @return Area
     */
    public function store(Request $request){
        $validation = $this->validate->store($request);
        $data       = $request->all();

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
            DB::beginTransaction();
        try {
            $data['codigo']     = strtoupper($data['codigo']);
            $codigo             = Area::where(['codigo' => $data['codigo'], 'inmueble_id' => $request->inmueble_id ])->first(); 

            if(!is_null($codigo))
                return response()->json(['error' => "El codigo ya se encuentra en uso, favor de cambiarlo."], 403);
            
            $item = Area::create($data);
            if(sizeof($request->colindancias)){
                foreach ($request->colindancias as $key => $colindancia) {
                    $col = new ConfiguracionesColindancia();
                    $col->area_id               = $item->id;
                    $col->colindancia_id        = $colindancia['colindancia_id'];
                    $col->tipo_colindancia_id   = $colindancia['tipo_colindancia_id'];
                    $col->cat_linea_id          = $colindancia['cat_linea_id'];
                    $col->cat_orientacion_id    = $colindancia['cat_orientacion_id'];
                    $col->distancia_colindancia = $colindancia['distancia_colindancia'];
                    $col->um_distancia_id       = $colindancia['um_distancia_id'];
                    $col->save();
                }
            }
           
            $this->saveParticulares($request, $item->id);
            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            return response()->json(['error' => $ex->getMessage()], 403);
        }
        
        return response()->json(['id'=> $item->id], 200);
    }

    /**
     * Retorna la vista de las areas validando los permisos del usuario logueado
     * @param Integer   $id
     * @return Object
     */
    public function getVista($id){
        $login      = Auth::user();
        $inmueble   = Inmueble::find($id);

        if( $login->rol->valor != 'root' && (is_null($inmueble) || $inmueble->cliente_id != $login->cliente_id) ){
            return response()->json( ['error'=> "Permisos denegados."], 403);
        }

        return view('configuraciones.areas.index', compact('inmueble'))->render();
    }

    /**
     * Retorna la vista de creación de un area, validando los permisos del usuario logueado
     * @param Request $request
     * @return View
     */
    public function create(Request $request){
        $login      = Auth::user();
        $area       = Area::find($request->id); 
        $inmueble   = Inmueble::find($request->inmueble_id);

        if( $login->rol->valor != 'root' && (is_null($inmueble) || $inmueble->cliente_id != $login->cliente_id) ){
            return response()->json( ['error'=> "Permisos denegados."], 403);
        }
        $accion = $request->accion;
        return view('configuraciones.areas.create', compact('inmueble', 'area','accion'))->render();
    }

    /**
     * Retorna un un array de los catalogos requeridos para crear un area
     * @param Integer   $inmueble_id
     * @return Array
     */
    public function getCatalogos($inmueble_id){
        $inmueble = Inmueble::find($inmueble_id);
        $catalogos = array();
        $catalogos['tipos']     = Area::where(['inmueble_id' => $inmueble->id, 'tipo' => 'general'])->orderBy('codigo')->get();
        $catalogos['catArea']   = CatalogosArea::where(['cliente_id' => $inmueble->cliente_id, 'activo' => true])->orderBy('clave')->get();
        $catalogos['catNivel']  = Nivel::where(['cliente_id' => $inmueble->cliente_id, 'activo' => true])->orderBy('clave')->get();
        $catalogos['catTipo']   = Tipo::where(['cliente_id' => $inmueble->cliente_id, 'activo' => true])->orderBy('clave')->get();
        $catalogos['catUso']    = Uso::where(['cliente_id' => $inmueble->cliente_id, 'activo' => true])->orderBy('clave')->get();
        $catalogos['catLinea']  = TipoLinea::where(['cliente_id' => $inmueble->cliente_id, 'activo' => true])->orderBy('clave')->get();
        $catalogos['catMedida'] = UnidadMedida::where(['cliente_id' => $inmueble->cliente_id, 'activo' => true])->orderBy('clave')->get();
        $catalogos['areas']     = Area::where('inmueble_id', $inmueble->id)->orderBy('codigo')->get();
        $catalogos['catColindancia']    = Colindancia::where(['cliente_id' => $inmueble->cliente_id, 'activo' => true])->orderBy('clave')->get();
        $catalogos['catOrientacion']    = Orientacion::where(['cliente_id' => $inmueble->cliente_id, 'activo' => true])->orderBy('clave')->get();
        $catalogos['catClasificacion']  = Clasificacion::where(['cliente_id' => $inmueble->cliente_id, 'activo' => true])->orderBy('clave')->get();

        return $catalogos;
    }

    /**
     * Retorna un Objeto tipo Area, si este existe en la db
     * @param Integer   $id
     * @return Object
     */
    public function get($id){
        $area = Area::with('colindancias')->where('id', $id)->get();
        if(sizeof($area) == 0)
            return response()->json( ['error'=> "No se encontro el registro con id ".$id], 403);
       
        return response()->json($area, 200);
    }

    /**
     * Actualiza un registro en la db
     * @param Request   $request
     * @return Boolean
     */
    public function update(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;

        try{
            $validation = $this->validate->update($request);
            $data = $request->all();

            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);
            
            DB::beginTransaction();
            $data['codigo']  = strtoupper($data['codigo']);
            $codigo          = Area::where(['codigo' => $data['codigo'], 'inmueble_id' => $request->inmueble_id])->where('id', "!=" , $data['id'])->first(); 

            if(!is_null($codigo))
                return response()->json(['error' => "El codigo ya se encuentra en uso, favor de cambiarlo."], 403);


            unset($data['colindancias']);
            unset($data['eliminar']);
            unset($data['array']);

            $this->saveParticulares($request, $request->id);
            Area::where('id', $request->id)->update($data);
            ConfiguracionesColindancia::where('area_id', $data['id'])->delete();

            if(sizeof($request->colindancias) > 0){
                foreach ($request->colindancias as $key => $value) {
                    $col = new ConfiguracionesColindancia();
                    $col->area_id               = $data['id'];
                    $col->colindancia_id        = $value['colindancia_id'];
                    $col->tipo_colindancia_id   = $value['tipo_colindancia_id'];
                    $col->cat_linea_id          = $value['cat_linea_id'];
                    $col->cat_orientacion_id    = $value['cat_orientacion_id'];
                    $col->distancia_colindancia = $value['distancia_colindancia'];
                    $col->um_distancia_id       = $value['um_distancia_id'];
                    $col->save();
                }
            }
            DB::commit();
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            DB::rollback();
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Elimina un registro en la db
     * @param Request   $request
     * @return Object
     */
    public function delete(Request $request){
        $item = Area::find($request->id);
        if(is_null($item))
            return response()->json( ['error'=> "No se encontro el registro con id ".$request->id], 403);
        
        $item->delete();
        return response()->json(['message'=> "Operacion exitosa. "], 200);
    }

    /**
     * Retorna un array de objeto tipo Area
     * @param Request   $request
     * @return Array
     */
    public function find(Request $request){
        $items      = [];
        $show       = 10;
        $order      = 'desc';
        $order_by   = 'a.id';

        $validation = $this->validate->find($request);
        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);

        $query = DB::table('areas as a')
                    ->leftJoin('inmuebles as i', 'i.id', '=', 'a.inmueble_id')
                    ->leftJoin('cat-areas as ca', 'ca.id', '=', 'a.cat_area_id')
                    ->leftJoin('cat-tipos as ct', 'ct.id', '=', 'a.cat_tipo_id')
                    ->leftJoin('cat-niveles as cn', 'cn.id', '=', 'a.cat_nivel_id')
                    ->leftJoin('cat-usos as cu', 'cu.id', '=', 'a.cat_uso_id')
                    ->leftJoin('cat-unidad_medidas as u', 'u.id', '=', 'a.unidad_medida_ce_id')
                    ->leftJoin('cat-clasificaciones as cl', 'cl.id', '=', 'a.cat_clasificacion_id')
                    ->leftJoin('cat-clasificaciones as cld', 'cld.id', '=', 'a.cat_clasificacion_dos_id')
                    ->select('a.*', 'ca.nombre as nom_area', 'ct.nombre as nom_tipo', 'cn.nombre as nom_nivel', 'cu.nombre as nom_uso', 'i.nombre as nom_inmueble',
                                'u.nombre as nombre_umce', 'cl.nombre as nombre_clasificacion', 'cld.nombre as nombre_clasificacion_dos');

        if(!is_null($request->inmueble_id))
            $query->where('a.inmueble_id', $request->inmueble_id);
        
        if(!is_null($request->cat_area_id))
            $query->where('a.cat_area_id', $request->cat_area_id);

        if(!is_null($request->cat_tipo_id))
            $query->where('a.cat_tipo_id', $request->cat_tipo_id);
        
        if(!is_null($request->cat_nivel_id))
            $query->where('a.cat_nivel_id', $request->cat_nivel_id);
            
        if(!is_null($request->cat_uso_id))
            $query->where('a.cat_uso_id', $request->cat_uso_id);

        if(!is_null($request->codigo))
            $query->where('a.codigo','ilike', '%'.$request->codigo.'%');

        if(!is_null($request->no))
            $query->where('a.no','ilike', '%'.$request->no.'%');

        if(!is_null($request->clase))
            $query->where('a.clase','ilike', '%'.$request->clase.'%');

        if(!is_null($request->tipo))
            $query->where('a.tipo', $request->tipo);
        
        /* Parametros para la paginanacion y el orden */
        if(!is_null($request->order))
            $order = $request->order;

        if(!is_null($request->order_by)){
            switch ($request->order_by) {
                case 'inmueble':
                    $order_by = 'i.nombre';
                    break;
                case 'codigo':
                    $order_by = 'a.codigo';
                    break;
                case 'area':
                    $order_by = 'ca.nombre';
                    break;
                case 'tipo':
                    $order_by = 'ct.nombre';
                    break;
                case 'clase':
                    $order_by = 'a.clase';
                    break;
                default:
                    $order_by = 'a.id';
                    break;
            }
        }

        $query->orderBy($order_by, $order); 
        if(is_null($request->paginate) || $request->paginate == "true" ){
            if(!is_null($request->show))
                $show = $request->show;
            
            $items = $query->paginate($show);
        }
        else{
            $items = $query->get();            
            return response()->json(["data" => $items], 200);
        }
        
        return response()->json($items, 200);
    }

    /**
     * Retorna un el codigo del area
     * @param Request   $request
     * @return String
     */
    public function getCodigo(Request $request){
        $validation = $this->validate->codigo($request);
        $codigo     = $request->no;

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);

        $area   = Area::where(['inmueble_id' => $request->inmueble_id, 'no' => $request->no])->orderBy('id', 'desc')->get(); 

        if(sizeof($area) > 0){
            if(sizeof($area) == 1)
                $codigo = $area[0]->codigo."-1";
            else{
                $array  = explode("-",$area[0]->codigo);
                $numero = (int)$array[sizeof($array) -1] + 1;
                $codigo = substr($area[0]->codigo, 0, -1).str_repeat($numero, 1);
            }
        }
        $result = ["codigo" => $codigo, "numero" => $area[0]->no];
        return response()->json($result, 200);
    }

    /**
     * Retorna un un array de las areas utilizada para la colindancia
     * @param Integer   $inmueble_id
     * @return Array
     */
    public function getColindancias($area_id){
        $area   = Area::find($area_id);
        $catalogos = array();
        $catalogos['colindancias']     = Area::where(['inmueble_id' => $area->inmueble->id])->where('id', '!=', $area_id)->orderBy('codigo')->get();
        return $catalogos;
    }

    /**
     * Valida si ya exite el codigo en el inmueble y obtiene las medidas capturadas en el inmueble
     * @param Request   $request
     * @return Array
     */
    public function validacion(Request $request){
        $totales    = ['terreno_exclusivo' => 0 , 'terreno_comun' => 0, 'construccion_comun' => 0, 'construccion_exclusiva' => 0];
        $areas      = Area::where(['inmueble_id' => $request->inmueble_id, 'tipo' => 'general'])->get();
        $existe     = Area::where(['inmueble_id' => $request->inmueble_id, 'tipo' => 'general', 'codigo' => $request->codigo])->first();

        foreach($areas as $area){ 
            $totales['terreno_exclusivo']       = $totales['terreno_exclusivo'] + $area->terreno_exclusivo;
            $totales['terreno_comun']           = $totales['terreno_comun'] + $area->terreno_comun;
            $totales['construccion_comun']      = $totales['construccion_comun'] + $area->construccion_comun;
            $totales['construccion_exclusiva']  = $totales['construccion_exclusiva'] + $area->construccion_exclusiva;
        }

        return response()->json(["totales" => $totales, "existe" => $existe], 200);
    }

    /**
     * Retorna las areas particulares de una area general
     * @param Request   $request
     * @return Array
     */
    public function getParticulares(Request $request){
        $validation = $this->validate->getParticulares($request);
        
        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
            
        $particulares   = Area::where(['general_id' => $request->id , 'tipo' => 'particular'])->orderBy('id', 'asc')->get();
        return $particulares;
    }

    /**
     * Crea, actualiza y elimina las areas de tipo particualr
     * @param Request   $request
     * @param Integer   $idArea
     * @return Boolean
     */
    public function saveParticulares(Request $request, $idArea){
        $success    = false;
        $validation = $this->validate->saveParticulares($request);

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
        try{
            Area::whereIn('id', $request->eliminar)->delete();
            $general = Area::find($idArea);
            $codigo  = 1;

            foreach ($request->array as $particular) {
                $new = Area::find($particular['id']);
                if(is_null($new))
                    $new = new Area();

                $new->tipo          = 'particular';
                $new->inmueble_id   = $general->inmueble_id;
                $new->cat_nivel_id  = $particular['cat_nivel_id'];
                $new->cat_tipo_id   = $particular['cat_tipo_id'];
                $new->cat_uso_id    = $particular['cat_uso_id'];
                $new->excluir       = $particular['excluir'];
                $new->general_id    = $idArea;
                $new->no_uso        = $particular['no_uso'];
                $new->codigo        = $general->codigo.'-'.strval($codigo);
                $new->no            = $general->no;
                $new->terreno_exclusivo         = $particular['terreno_exclusivo'];
                $new->unidad_medida_ce_id       = $particular['unidad_medida_ce_id'];
                $new->construccion_exclusiva    = $particular['construccion_exclusiva'];
                $new->cat_clasificacion_dos_id  = $particular['cat_clasificacion_dos_id'];
                $new->cat_clasificacion_id      = $particular['cat_clasificacion_id'];
                $new->save();

                $codigo++;
            }

            $success = true;
        }
        catch(Exception $ex){
            throw $ex;
        }
        
        return response()->json(['message'=> $success], 200);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\ClienteValidator;


class ClienteController extends Controller
{
    protected $validate;
    public function __construct() {
        $this->validate = new ClienteValidator();
    }


    /**
     * Retorna un array de objeto tipo usuario
     * @param Request   $request
     * @return Array
     */
    public function find(Request $request){
        $items      = [];
        $show       = 10;
        $order      = 'desc';
        $order_by   = 'id';

        $validation = $this->validate->find($request);
        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);

        $query = DB::table('clientes');

        if(!is_null($request->nombre))
            $query->where('nombre','ilike', '%'.$request->nombre.'%');

        if(!is_null($request->correo))
            $query->where('correo','ilike', '%'.$request->correo.'%');

        if(!is_null($request->activo)){
            $activo =  $request->activo == 'true' ? true : false;
            $query->where('activo', $activo);
        }
        
         /* Parametros para la paginanacion y el orden */
        if(!is_null($request->order))
            $order = $request->order;

        if(!is_null($request->order_by))
            $order_by = $request->order_by;
            
        $query->orderBy($order_by, $order); 
       
        if(is_null($request->paginate) || $request->paginate == "true" ){
            if(!is_null($request->show))
                $show = $request->show;
            
            $items = $query->paginate($show);
        }
        else{
            $items = $query->get();            
            return response()->json(["data" => $items], 200);
        }
        
        return response()->json($items, 200);
    }
}

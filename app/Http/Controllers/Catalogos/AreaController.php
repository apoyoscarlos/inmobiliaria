<?php

namespace App\Http\Controllers\Catalogos;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Catalogos\Area;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests\Catalogos\AreaValidator;

class AreaController extends Controller
{
    protected $validate;
    public function __construct() {
        $this->validate = new AreaValidator();
    }

    /**
     * Crea un registro en la db
     * @param Request   $request
     * @return Area
     */
    public function store(Request $request){
        $validation = $this->validate->store($request);
        $data       = $request->all();

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
        $data['clave']      = strtoupper($data['clave']);
        $clave = Area::where(['clave' => $data['clave'], 'cliente_id' => $data['cliente_id']])->first();
        
        if(!is_null($clave))
            return response()->json(['error'=> 'La clave ya se encuentra en uso.'], 403);
        
        $item = Area::create($data);
        return response()->json(['id'=> $item->id], 200);
    }

    /**
     * Retorna un Objeto tipo Area, si este existe en la db
     * @param Integer   $id
     * @return Object
     */
    public function get($id){
        $item = Area::find($id);
        if(is_null($item))
            return response()->json( ['error'=> "No se encontro el registro con id ".$id], 403);
       
        return response()->json($item, 200);
    }

    /**
     * Actualiza un registro en la db
     * @param Request   $request
     * @return Boolean
     */
    public function update(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->update($request);
            $data = $request->all();

            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);
            
            $data['clave']  = strtoupper($data['clave']);
            $clave          = Area::where(['clave' => $data['clave'], 'cliente_id' => $request->cliente_id])->first();
        
            if(!is_null($clave) && $clave->id != $request->id)
                return response()->json(['error'=> 'La clave ya se encuentra en uso.'], 403);
            
            
            Area::where('id', $request->id)->update($data);
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Elimina o deshabilita un registro en la db
     * @param Request   $request
     * @return Object
     */
    public function delete(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->delete($request);
            $data = $request->all();

            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);

            $item = Area::find($request->id);
            if($request->accion == 'delete')
                $item->delete();
            else{
                $item->activo = !$item->activo;
                $item->save();
            }
            
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Retorna un array de objeto tipo Area
     * @param Request   $request
     * @return Array
     */
    public function find(Request $request){
        $items      = [];
        $show       = 10;
        $order      = 'desc';
        $order_by   = 'a.id';

        $validation = $this->validate->find($request);
        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);

        $query = DB::table('cat-areas as a')
                    ->leftJoin('clientes as c', 'c.id', '=', 'a.cliente_id')
                    ->select('a.*', 'c.nombre as nom_cliente', 'c.apellido as cli_apellido');

        if(!is_null($request->cliente_id)){
            $query->where('a.cliente_id', $request->cliente_id);
        }

        if(!is_null($request->clave))
            $query->where('a.clave','ilike', '%'.$request->clave.'%');

        if(!is_null($request->nombre))
            $query->where('a.nombre','ilike', '%'.$request->nombre.'%');

        if(!is_null($request->descripcion))
            $query->where('a.descripcion','ilike', '%'.$request->descripcion.'%');

        if(!is_null($request->activo)){
            $activo =  $request->activo == 'true' ? true : false;
            $query->where('a.activo', $activo);
        }
        
        /* Parametros para la paginanacion y el orden */
        if(!is_null($request->order))
            $order = $request->order;

        if(!is_null($request->order_by)){
            $order_by = 'a.'.$request->order_by;
            
            if($request->order_by == 'cliente')
                $order_by = 'c.nombre';
        }

        $query->orderBy($order_by, $order); 
        if(is_null($request->paginate) || $request->paginate == "true" ){
            if(!is_null($request->show))
                $show = $request->show;
            
            $items = $query->paginate($show);
        }
        else{
            $items = $query->get();            
            return response()->json(["data" => $items], 200);
        }
        
        return response()->json($items, 200);
    }
}

<?php

namespace App\Http\Controllers\Catalogos;

use Exception;
use App\Models\User;
use App\Models\Cliente;
use App\Models\usuariosubscripcion;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Catalogos\Subscripcion;
use App\Http\Requests\Catalogos\SubscripcionValidator;

use App\Http\Requests\UsuarioValidator;

class subscripcionController extends Controller
{
    protected $validate;
    protected $validateUser;
    public function __construct() {
        $this->validate = new SubscripcionValidator();
        $this->validateUser = new UsuarioValidator();
    
    }

    /**
     * Crea un registro en la db
     * @param Request   $request
     * @return Object
     */
    public function store(Request $request){
        $validation = $this->validate->store($request);
        $data = $request->all();

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
            
        $data['nombre']      = strtoupper($data['nombre']);
        $clave = subscripcion::where(['nombre' => $data['nombre'], 'cliente_id' => $data['cliente_id']])->first();
        
        if(!is_null($clave))
            return response()->json(['error'=> 'La clave ya se encuentra en uso.'], 403);

        $item = subscripcion::create($data);
        return response()->json(['id'=> $item->id], 200);
    }

    /**
     * Retorna un Objeto tipo tipo de unidad de medida, si este existe en la db
     * @param Integer   $id
     * @return subscripcion
     */
    public function get($id){
        $item = subscripcion::find($id);
        if(is_null($item))
            return response()->json( ['error'=> "No se encontro el registro con id ".$id], 403);
       
        return response()->json($item, 200);
    }

    /**
     * Actualiza un registro en la db
     * @param Request   $request
     * @return Boolean
     */
    public function update(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->update($request);
            $data = $request->all();

            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);

            $data['nombre']  = strtoupper($data['nombre']);
            $clave          = subscripcion::where(['nombre' => $data['nombre'], 'cliente_id' => $data['cliente_id']])->first();
        
            if(!is_null($clave) && $clave->id != $request->id)
                return response()->json(['error'=> 'El nombre ya se encuentra en uso.'], 403);

            subscripcion::where('id', $request->id)->update($data);
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Elimina o deshabilita un registro en la db
     * @param Request   $request
     * @return Boolean
     */
    public function delete(Request $request){
        $response = array('message' => 'Error');
        $codigo = 403;
        
        try{
            $validation = $this->validate->delete($request);
            $data = $request->all();

            if( $validation  !== true)
                return response()->json(['error'=> $validation->original], 403);

            $item = subscripcion::find($request->id);
            if($request->accion == 'delete')
                $item->delete();
            else{
                $item->activo = !$item->activo;
                $item->save();
            }
            
            $response["message"] = "Operacion Exitosa";
            $codigo = 200;
        }
        catch(Exception $ex){
            $response["message"] = $ex->getMessage();
        }
        return response()->json($response, $codigo);
    }

    /**
     * Retorna un array de objeto tipo de unida de medida
     * @param Request   $request
     * @return Array
     */
    public function find(Request $request){
        $items      = [];
        $show       = 10;
        $order      = 'desc';
        $order_by   = 'subs.id';

        $validation = $this->validate->find($request);
        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);

        $query = DB::table('subscripciones as subs')
                     ->leftJoin('clientes as c', 'c.id', '=', 'subs.cliente_id')
                    ->select('subs.*', 'c.nombre as nom_cliente', 'c.apellido as cli_apellido');

        if(!is_null($request->cliente_id))
            $query->where('subs.cliente_id', $request->cliente_id);
        

        if(!is_null($request->nombre))
            $query->where('subs.nombre','ilike', '%'.$request->nombre.'%');

        if(!is_null($request->descripcion))
            $query->where('subs.descripcion','ilike', '%'.$request->descripcion.'%');

        if(!is_null($request->activo)){
            $activo =  $request->activo == 'true' ? true : false;
            $query->where('subs.activo', $activo);
        }
        
        /* Parametros para la paginanacion y el orden */
        if(!is_null($request->order))
            $order = $request->order;

            if(!is_null($request->order_by)){
                $order_by = 'subs.'.$request->order_by;
                
                if($request->order_by == 'cliente')
                    $order_by = 'c.nombre';
            }
        
        $query->orderBy($order_by, $order); 
       
        if(is_null($request->paginate) || $request->paginate == "true" ){
            if(!is_null($request->show))
                $show = $request->show;
            
            $items = $query->paginate($show);
        }
        else{
            $items = $query->get();            
            return response()->json(["data" => $items], 200);
        }
        
        return response()->json($items, 200);
    }

    public function comprarpaquete(Request $request){

        $subs  = $request->subs;
        $cliente   = $request->cliente;

        $requestcliente = new Request($cliente);
        $requestsubs = new Request($subs);
      
        $validation = $this->validateUser->store($requestcliente);
        $data = $requestcliente->all();

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
        $data['correo']     = strtolower($data['correo']);
        $data['password']   = bcrypt($requestcliente->password);
        $cliente            = Cliente::create($data);
        $data['cliente_id'] = $cliente->id;
        $item               = User::create($data);

        $dataSub = $requestsubs->all();

        $userSub['cliente_id']  = $cliente->id;
        $userSub['subscripcion_id']  = $dataSub['id'];
        $userSub['creditos']    = $dataSub['creditos'];
        $userSub['activo']      = true;

        $itemsub           = usuariosubscripcion::create($userSub);
        
        return response()->json(['id'=> $item->id], 200);
        
    }
}

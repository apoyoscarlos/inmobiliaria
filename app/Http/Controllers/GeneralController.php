<?php

namespace App\Http\Controllers;

use Exception;
use Illuminate\Http\Request;
use App\Exports\CatalogosExport;
use App\Imports\CatalogosImport;
use App\Library\ConversorNumerico;
use Maatwebsite\Excel\Facades\Excel;
use App\Http\Requests\GeneralValidator;
use App\Http\Controllers\Catalogos\UsoController;
use App\Http\Controllers\Catalogos\AreaController;
use App\Http\Controllers\Catalogos\TipoController;
use App\Http\Controllers\Catalogos\NivelController;
use App\Http\Controllers\Catalogos\TipoLineaController;
use App\Http\Controllers\Catalogos\ColindanciaController;
use App\Http\Controllers\Catalogos\OrientacionController;
use App\Http\Controllers\Catalogos\UnidadMedidaController;
use App\Http\Controllers\Catalogos\ClasificacionController;

class GeneralController extends Controller
{
    /**
     * Importa la informacion proporcionada en un excel
     * @param Request   $request
     * @return String
     */
    public function importExcel(Request $request){
        $validate   = new GeneralValidator();
        $validation = $validate->importar($request);

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
            try {
                $file   = $request->file('file')->store('import');
                $import = new CatalogosImport($request->tipo, $request->cliente_id); 
                Excel::import($import, $file);
            } catch (Exception $ex) {
                return response()->json(['error'=> $ex], 403);
            }
        return response()->json(["message" => "Operacion Exitosa"], 200);
    }

    /**
     * Retorna un archivo excel con los registros de los catalogos solicitados.
     * @param Request   $request
     * @return Excel
     */
    public function exportExcel(Request $request){
        $datos      = [];
        $nombre     = $request->tipo.'.'.$request->extension;
        $validate   = new GeneralValidator();
        $validation = $validate->exportar($request);

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
        switch ($request->tipo) {
            case 'cat-areas':
                $instancia  = new AreaController();
                $respuesta  = $instancia->find($request);
                $datos      = $respuesta->getData()->data;
                break;
            case 'cat-niveles':
                $instancia  = new NivelController();
                $respuesta  = $instancia->find($request);
                $datos      = $respuesta->getData()->data;
                break;
            case 'cat-orientaciones':
                $instancia  = new OrientacionController();
                $respuesta  = $instancia->find($request);
                $datos      = $respuesta->getData()->data;
                break;
            case 'cat-tipos':
                $instancia  = new TipoController();
                $respuesta  = $instancia->find($request);
                $datos      = $respuesta->getData()->data;
                break;
            case 'cat-tipo-lineas':
                $instancia  = new TipoLineaController();
                $respuesta  = $instancia->find($request);
                $datos      = $respuesta->getData()->data;
                break;
            case 'cat-unidad-medidas':
                $instancia  = new UnidadMedidaController();
                $respuesta  = $instancia->find($request);
                $datos      = $respuesta->getData()->data;
                break;
            case 'cat-usos':
                $instancia  = new UsoController();
                $respuesta  = $instancia->find($request);
                $datos      = $respuesta->getData()->data;
                break;
            case 'cat-colindancia':
                $instancia  = new ColindanciaController();
                $respuesta  = $instancia->find($request);
                $datos      = $respuesta->getData()->data;
                break;
            case 'cat-clasificacion':
                $instancia  = new ClasificacionController();
                $respuesta  = $instancia->find($request);
                $datos      = $respuesta->getData()->data;
                break;
            default:
                # code...
                break;
        }

        $rol = 'cliente';
        if(is_null($request->cliente_id))
            $rol = 'admin';
        
        return Excel::download(new CatalogosExport($request->tipo, $datos, $rol), $nombre);
    }

    /**
     * Retorna el valor de un numero en texto.
     * @param Request   $request
     * @return String
     */
    public function conversorNumerico(Request $request){
        $validate   = new GeneralValidator();
        $validation = $validate->conversor($request);

        if( $validation  !== true)
            return response()->json(['error'=> $validation->original], 403);
        
        $conversor = new ConversorNumerico();
        return $conversor->convertir($request->numero);
    }

    /**
     * Retorna un archivo word.
     * @param Request   $request
     * @return String
     */
    public function generarWord(Request $request){
        // CREAR CARPETA
        $carpeta = storage_path()."/archivos/";
        if(!file_exists($carpeta))
            mkdir($carpeta, 0777, true);

        $phpWord = new \PhpOffice\PhpWord\PhpWord();
        $section = $phpWord->addSection();
        $section->addImage("https://www.itsolutionstuff.com/frontTheme/images/logo.png");
        $section->addText($request->texto);

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
        try {
            $path  = $carpeta."/archivo_".date('d_m_y')."_".date('G_i_s.docx');
            $objWriter->save($path);
        } catch (Exception $ex) {
            throw $ex;
        }

        return response()->download($path)->deleteFileAfterSend();
    }
}

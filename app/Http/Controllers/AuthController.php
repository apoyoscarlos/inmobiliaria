<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AuthController extends Controller
{
    /**
     * Funcion para autentificarse 
     * @param Request   $request
     * @return Boolean
     */
    public function login(Request $request){

        if(Auth::attempt(['correo' => $request->username, 'password' => $request->password, 'activo' => true ])){
            request()->session()->regenerate();
            return redirect()->route('dashboard');
        }
            
        return back()
            ->withErrors(['username' => trans('auth.failed')])
            ->withInput(request(['username'])); 
    }

    /**
     * Elimna las session del usuario autenticado 
     * @param Request   $request
     * @return Boolean
     */ 
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        //$request->user()->token()->revoke();

        return redirect()->route('login');;
    }   
}

<?php

namespace App\Imports;

use Exception;
use App\Models\User;
use App\Models\Catalogos\Uso;
use App\Models\Catalogos\Area;
use App\Models\Catalogos\Tipo;
use App\Models\Catalogos\Nivel;
use Illuminate\Support\Collection;
use App\Models\Catalogos\TipoLinea;
use App\Models\Catalogos\Colindancia;
use App\Models\Catalogos\Orientacion;
use App\Models\Catalogos\UnidadMedida;
use App\Models\Catalogos\Clasificacion;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;


class CatalogosImport implements ToCollection, WithHeadingRow
{
    protected $tipo;
    protected $cliente_id;
    public function __construct($tipo, $cliente_id){
        $this->tipo         = $tipo;
        $this->cliente_id   = $cliente_id;
    }
    /**
    * @param Collection $collection
    */
    public function collection(Collection $collection)
    {
        try{
            foreach ($collection as $row) {
                if($row['clave'] != "" && !is_null($row['clave'])){
                    $clave  = strtoupper($row['clave']);
                    $item   = null;
    
                    switch ($this->tipo) {
                        case 'cat-areas':
                            $item = Area::where(['clave' => $clave, 'cliente_id' => $this->cliente_id])->first();
                            break;
                        case 'cat-niveles':
                            $item = Nivel::where(['clave' => $clave, 'cliente_id' => $this->cliente_id])->first();
                            break;
                        case 'cat-orientaciones':
                            $item = Orientacion::where(['clave' => $clave, 'cliente_id' => $this->cliente_id])->first();
                            break;
                        case 'cat-tipo-lineas':
                            $item = TipoLinea::where(['clave' => $clave, 'cliente_id' => $this->cliente_id])->first();
                            break;
                        case 'cat-unidad-medidas':
                            $item = UnidadMedida::where(['clave' => $clave, 'cliente_id' => $this->cliente_id])->first();
                            break;
                        case 'cat-tipos':
                            $item = Tipo::where(['clave' => $clave, 'cliente_id' => $this->cliente_id])->first();
                            break;
                        case 'cat-usos':
                            $item = Uso::where(['clave' => $clave, 'cliente_id' => $this->cliente_id])->first();
                            break;
                        case 'cat-colindancia':
                            $item = Colindancia::where(['clave' => $clave, 'cliente_id' => $this->cliente_id])->first();
                            break;
                        case 'cat-clasificacion':
                            $item = Clasificacion::where(['clave' => $clave, 'cliente_id' => $this->cliente_id])->first();
                            break;
                    }
    
                    if(is_null($item)){
                        switch ($this->tipo) {
                            case 'cat-areas':
                                $item = new Area();
                                break;
                            case 'cat-niveles':
                                $item = new Nivel();
                                break;
                            case 'cat-orientaciones':
                                $item = new Orientacion();
                                break;
                            case 'cat-tipo-lineas':
                                $item = new TipoLinea();
                                break;
                            case 'cat-unidad-medidas':
                                $item = new UnidadMedida();
                                break;
                            case 'cat-tipos':
                                $item = new Tipo();
                                break;
                            case 'cat-usos':
                                $item = new Uso();
                                break;
                            case 'cat-colindancia':
                                $item = new Colindancia();
                                break;
                            case 'cat-clasificacion':
                                $item = new Clasificacion();
                                break;
                        }
                    }
    
                    $item->cliente_id   = $this->cliente_id;
                    $item->clave        = strtoupper($row['clave']);
                    $item->nombre       = $row['nombre'];
                    $item->descripcion  = $row['descripcion'];
                    $item->save();
                }
            }
        }
        catch(Exception $ex){
            return $ex;
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Cliente extends Model
{
    use HasFactory;
    protected $table = 'clientes';
    protected $fillable =  [
        'nombre',
        'apellido',
        'telefono',
        'correo',
        'activo'
    ];

    public function usuarios(): HasMany{
        return $this->hasMany(User::class);
    }
}

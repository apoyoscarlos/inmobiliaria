<?php

namespace App\Models\Configuraciones;

use App\Models\Catalogos\Uso;
use App\Models\Catalogos\Tipo;
use App\Models\Catalogos\Nivel;
use App\Models\Catalogos\UnidadMedida;
use Illuminate\Database\Eloquent\Model;
use App\Models\Configuraciones\Inmueble;
use App\Models\Catalogos\Area as CatalogosArea;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Area extends Model
{
    use HasFactory;
    protected $table = 'areas';
    protected $fillable =  [
        'tipo',
        'inmueble_id',
        'cat_area_id',
        'cat_tipo_id',
        'cat_nivel_id',
        'cat_uso_id',
        'excluir',
        'codigo',
        'no',
        'no_uso',
        'terreno_exclusivo',
        'terreno_comun',
        'construccion_exclusiva',
        'unidad_medida_ce_id',
        'construccion_comun',
        'clase',
        'cuota_participacion',
        'comentarios',
        'general_id',
        'cat_clasificacion_id',
        'cat_clasificacion_dos_id'
    ];

    public function inmueble() : BelongsTo{
        return $this->belongsTo(Inmueble::class);
    }

    public function area() : BelongsTo{
        return $this->belongsTo(CatalogosArea::class, 'cat_area_id');
    }

    public function tipo() : BelongsTo{
        return $this->belongsTo(Tipo::class, 'cat_tipo_id');
    }

    public function nivel() : BelongsTo{
        return $this->belongsTo(Nivel::class, 'cat_nivel_id');
    }

    public function uso() : BelongsTo{
        return $this->belongsTo(Uso::class, 'cat_uso_id');
    }

    public function unidad_medida_ce() : BelongsTo{
        return $this->belongsTo(UnidadMedida::class, 'unidad_medida_ce_id');
    }

    public function colindancias() : HasMany {
        return $this->hasMany(Colindancia::class, 'area_id');
    }

    public function general() : BelongsTo{
        return $this->belongsTo(Area::class, 'general_id');
    }
}

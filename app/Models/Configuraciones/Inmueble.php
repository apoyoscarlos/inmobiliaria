<?php

namespace App\Models\Configuraciones;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Inmueble extends Model
{
    use HasFactory;
    protected $table = 'inmuebles';
    protected $fillable =  [
        'cliente_id',
        'generado',
        'nombre',
        'municipio',
        'colonia',
        'calle',
        'numero',
        'descripcion',
        'terreno_exclusivo',
        'terreno_comun',
        'constuccion_exclusiva',
        'constuccion_comun',
    ];

    public function cliente() : BelongsTo{
        return $this->belongsTo(Cliente::class);
    }

    public function areas() : HasMany{
        return $this->hasMany(Area::class);
    }
}

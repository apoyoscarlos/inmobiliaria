<?php

namespace App\Models\Configuraciones;

use App\Models\Catalogos\TipoLinea;
use App\Models\Configuraciones\Area;
use App\Models\Catalogos\Orientacion;
use App\Models\Catalogos\UnidadMedida;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\Catalogos\Colindancia as CatalogosColindancia;

class Colindancia extends Model
{
    use HasFactory;
    protected $table = 'colindancias';
    protected $fillable =  [
        'area_id',
        'colindancia_id',
        'tipo_colindancia_id',
        'cat_linea_id',
        'cat_orientacion_id',
        'distancia_colindancia',
        'um_distancia_id'
    ];

    public function area() : BelongsTo{
        return $this->belongsTo(Area::class);
    }

    public function tipoColindancia() : BelongsTo{
        return $this->belongsTo(CatalogosColindancia::class,'tipo_colindancia_id');
    }

    public function colindancia() : BelongsTo{
        return $this->belongsTo(Area::class,'colindancia_id');
    }

    public function linea() : BelongsTo{
        return $this->belongsTo(TipoLinea::class,'cat_linea_id');
    }

    public function orientacion() : BelongsTo{
        return $this->belongsTo(Orientacion::class, 'cat_orientacion_id');
    }

    public function unidadDistancia() : BelongsTo{
        return $this->belongsTo(UnidadMedida::class, 'um_distancia_id');
    }
}

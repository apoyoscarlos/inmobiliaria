<?php

namespace App\Models\Catalogos;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Clasificacion extends Model
{
    use HasFactory;
    protected $table = 'cat-clasificaciones';
    protected $fillable =  [
        'cliente_id',
        'clave',
        'nombre',
        'descripcion',
        'activo'
    ];

    public function cliente() : BelongsTo{
        return $this->belongsTo(Cliente::class);
    }
}

<?php

namespace App\Models\Catalogos;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Area extends Model
{
    use HasFactory;
    protected $table = 'cat-areas';
    protected $fillable =  [
        'cliente_id',
        'clave',
        'nombre',
        'descripcion',
        'activo'
    ];

    public function cliente() : BelongsTo{
        return $this->belongsTo(Cliente::class);
    }
}

<?php

namespace App\Models\Catalogos;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Subscripcion extends Model
{
    use HasFactory;
    protected $table = 'subscripciones';
    protected $fillable =  [
        'cliente_id',
        'nombre',
        'descripcion',
        'gratuito',
        'costo',
        'creditos',
        'activo'
    ];

    public function cliente() : BelongsTo{
        return $this->belongsTo(Cliente::class);
    }
}

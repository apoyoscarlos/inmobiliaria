<?php

namespace App\Models;

use App\Models\Cliente;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class usuariosubscripcion extends Model
{
    use HasFactory;
    protected $table = 'usuariosubscripcion';
    protected $fillable =  [
        'cliente_id',
        'subscripcion_id',
        'creditos',
        'activo'
    ];

    public function cliente() : BelongsTo{
        return $this->belongsTo(Cliente::class);
    }

    public function subscripcion() : BelongsTo{
        return $this->belongsTo(Subscripcion::class);
    }

}

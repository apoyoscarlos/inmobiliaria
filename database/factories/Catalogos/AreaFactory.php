<?php

namespace Database\Factories\Catalogos;

use Illuminate\Support\Str;
use App\Models\Catalogos\Area;
use Illuminate\Database\Eloquent\Factories\Factory;

class AreaFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Area::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'clave'         => Str::random(10),
            'nombre'        => $this->faker->name,
            'descripcion'   => $this->faker->sentence
        ];
    }
}

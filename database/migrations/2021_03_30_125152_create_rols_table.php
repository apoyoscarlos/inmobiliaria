<?php

use App\Models\Rol;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->id();
            $table->string('nombre');
            $table->string('valor');
            $table->string('descripcion')->nullable();
            $table->boolean('activo')->default(true);
            $table->timestamps();
        });

        //Se crean los roles
        $rol            = new Rol();
        $rol->nombre    = "Super Usuario";
        $rol->valor     = "root";
        $rol->descripcion = "Rol utilizado por el dueño del sistemas y desarrolladores";
        $rol->save();

        $rol            = new Rol();
        $rol->nombre    = "Administrador";
        $rol->valor     = "admin";
        $rol->descripcion = "Rol utilizado por los adminsitradores";
        $rol->save();

        $rol            = new Rol();
        $rol->nombre    = "Usuario";
        $rol->valor     = "usuario";
        $rol->descripcion = "Rol utilizado por los usuarios asignados por el administrador";
        $rol->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}

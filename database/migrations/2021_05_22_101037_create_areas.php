<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('areas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('inmueble_id')->nullable()->unsigned()->index();
            $table->bigInteger('cat_area_id')->nullable()->unsigned()->index();
            $table->bigInteger('cat_tipo_id')->nullable()->unsigned()->index();
            $table->bigInteger('cat_nivel_id')->nullable()->unsigned()->index();
            $table->bigInteger('cat_uso_id')->nullable()->unsigned()->index();
            $table->bigInteger('general_id')->nullable()->unsigned()->index();
            $table->bigInteger('cat_clasificacion_id')->nullable()->unsigned()->index();
            $table->bigInteger('cat_clasificacion_dos_id')->nullable()->unsigned()->index();

            $table->enum('tipo', ['general', 'particular'])->default('general');
            $table->boolean('excluir')->default(false);
            $table->string('codigo',50)->nullable();
            $table->string('no',50)->nullable();
            $table->string('no_uso',50)->nullable();
            $table->float('terreno_exclusivo', 6, 2)->nullable();
            $table->float('terreno_comun', 6, 2)->nullable();
            $table->float('construccion_exclusiva', 6, 2)->nullable();
            $table->bigInteger('unidad_medida_ce_id')->nullable()->unsigned()->index();
            $table->float('construccion_comun', 6, 2)->nullable();
            $table->string('clase',250)->nullable();
            $table->float('cuota_participacion', 6, 6)->nullable();
            $table->text('comentarios')->nullable();
            $table->timestamps();

            $table->foreign('inmueble_id')->references('id')->on('inmuebles')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('cat_area_id')->references('id')->on('cat-areas')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('cat_tipo_id')->references('id')->on('cat-tipos')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('cat_nivel_id')->references('id')->on('cat-niveles')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('cat_uso_id')->references('id')->on('cat-usos')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('unidad_medida_ce_id')->references('id')->on('cat-unidad_medidas')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('general_id')->references('id')->on('areas')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('cat_clasificacion_id')->references('id')->on('cat-clasificaciones')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('cat_clasificacion_dos_id')->references('id')->on('cat-clasificaciones')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
                    
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('areas');
    }
}

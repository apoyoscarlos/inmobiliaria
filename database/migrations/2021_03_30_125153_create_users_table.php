<?php

use App\Models\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cliente_id')->nullable()->unsigned()->index();
            $table->bigInteger('rol_id')->nullable()->unsigned()->index();
            $table->string('nombre', 200)->nullable();
            $table->string('apellido', 200)->nullable();
            $table->string('telefono', 45)->nullable();
            $table->string('correo', 200)->nullable();
            $table->string('password', 200)->nullable();
            $table->boolean('activo')->default(true);
            $table->timestamps();

            $table->foreign('cliente_id')->references('id')->on('clientes')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('rol_id')->references('id')->on('roles')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
        });

        // Creo usuaario base
        $usuario            = new User();
        $usuario->rol_id    = 1;
        $usuario->nombre    = "Manuel";
        $usuario->apellido  = "Euan";
        $usuario->telefono  = "9993381472";
        $usuario->correo    = "admin@gmail.com";
        $usuario->password = bcrypt("123456");
        $usuario->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}

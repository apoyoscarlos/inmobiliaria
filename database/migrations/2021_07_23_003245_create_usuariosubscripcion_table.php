<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsuariosubscripcionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuariosubscripcion', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cliente_id')->nullable()->unsigned()->index();
            $table->bigInteger('subscripcion_id')->nullable()->unsigned()->index();
            $table->bigInteger('creditos');
            $table->boolean('activo')->default(true);
            $table->timestamps();

            $table->foreign('cliente_id')->references('id')->on('clientes')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");

                    $table->foreign('subscripcion_id')->references('id')->on('subscripciones')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuariosubscripcion');
    }
}

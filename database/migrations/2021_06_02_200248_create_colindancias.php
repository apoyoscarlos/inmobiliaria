<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateColindancias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('colindancias', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('area_id')->unsigned()->index();
            $table->bigInteger('colindancia_id')->unsigned()->index();
            $table->bigInteger('tipo_colindancia_id')->unsigned()->index();
            $table->bigInteger('cat_linea_id')->unsigned()->index();
            $table->bigInteger('cat_orientacion_id')->unsigned()->index();
        
            $table->float('distancia_colindancia', 6, 2)->nullable();
            $table->bigInteger('um_distancia_id')->nullable()->unsigned()->index();
            $table->timestamps();
        
            $table->foreign('area_id')->references('id')->on('areas')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('colindancia_id')->references('id')->on('areas')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('tipo_colindancia_id')->references('id')->on('cat-colindancias')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('cat_linea_id')->references('id')->on('cat-tipo_lineas')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
            $table->foreign('cat_orientacion_id')->references('id')->on('cat-orientaciones')
                    ->onDelete("cascade")
                    ->onUpdate("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('colindancias');
    }
}

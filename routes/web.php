<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\Configuraciones\AreaControler;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/login', 'auth.login')->name('login');
Route::group(['prefix' => 'auth'], function () {
    Route::post('/login', [AuthController::class, 'login'])->middleware('guest');
    Route::post('/logout', [AuthController::class,'logout'])->middleware('auth')->name('logout');
});

Route::group(['middleware' => ['auth']], function () {
    Route::view('/', 'welcome')->name('dashboard');

    Route::group(['prefix' => 'catalogos'], function () {
        Route::view('/orientaciones', 'catalogos.Orientacion')->name('cat-orientaciones');
        Route::view('/unidadDeMedida', 'catalogos.UnidadDeMedida')->name('cat-unidad-de-medidas');
        Route::view('/areas', 'catalogos.Area')->name('cat-areas');
        Route::view('/tipos', 'catalogos.Tipo')->name('cat-tipos');
        Route::view('/niveles', 'catalogos.Nivel')->name('cat-niveles');
        Route::view('/usos', 'catalogos.Uso')->name('cat-usos');
        Route::view('/lineas', 'catalogos.TipoLinea')->name('cat-tipo-lineas');
        Route::view('/clasificaciones', 'catalogos.Clasificacion')->name('cat-clasificaciones');
        Route::view('/colindancias', 'catalogos.Colindancia')->name('cat-colindancia');
        Route::view('/subscripciones', 'catalogos.Subscripcion')->name('subscripcion');
    });
    
    Route::view('/paquetes', 'Paquetes')->name('paquetes');
    Route::view('/usuarios', 'Usuario')->name('usuarios');
    Route::view('/inmuebles', 'configuraciones.Inmuebles')->name('inmuebles');
    
    Route::group(['prefix' => 'areas'], function () {
        Route::get('/create', [AreaControler::class, 'create']);
        Route::get('/{id}', [AreaControler::class, 'getVista']);
    });
});












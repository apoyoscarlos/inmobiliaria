<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ClienteController;
use App\Http\Controllers\GeneralController;
use App\Http\Controllers\UsuarioController;
use App\Http\Controllers\Catalogos\UsoController;
use App\Http\Controllers\Catalogos\AreaController;
use App\Http\Controllers\Catalogos\TipoController;
use App\Http\Controllers\Catalogos\NivelController;
use App\Http\Controllers\Catalogos\TipoLineaController;
use App\Http\Controllers\Configuraciones\AreaControler;
use App\Http\Controllers\Catalogos\ColindanciaController;
use App\Http\Controllers\Catalogos\OrientacionController;
use App\Http\Controllers\Catalogos\SubscripcionController;
use App\Http\Controllers\Catalogos\UnidadMedidaController;
use App\Http\Controllers\Catalogos\ClasificacionController;
use App\Http\Controllers\Configuraciones\InmuebleController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::post('/conversorNumerico', [GeneralController::class, 'conversorNumerico']);
Route::post('/generarWord', [GeneralController::class, 'generarWord']);
Route::post('/importar', [GeneralController::class, 'importExcel']);
Route::post('/exportar', [GeneralController::class, 'exportExcel']);

Route::group(['prefix' => 'catalogos'], function () {
    Route::group(['prefix' => 'area'], function () {
        Route::post('/', [AreaController::class, 'store']);
        Route::put('/', [AreaController::class,'update']);
        Route::post('/delete', [AreaController::class, 'delete']);
        Route::get('/find', [AreaController::class, 'find']);
        Route::get('/{id}', [AreaController::class, 'get']);
    });

    Route::group(['prefix' => 'nivel'], function () {
        Route::post('/', [NivelController::class, 'store']);
        Route::put('/', [NivelController::class,'update']);
        Route::post('/delete', [NivelController::class, 'delete']);
        Route::get('/find', [NivelController::class, 'find']);
        Route::get('/{id}', [NivelController::class, 'get']);
    });

    Route::group(['prefix' => 'orientacion'], function () {
        Route::post('/', [OrientacionController::class, 'store']);
        Route::put('/', [OrientacionController::class,'update']);
        Route::post('/delete', [OrientacionController::class, 'delete']);
        Route::get('/find', [OrientacionController::class, 'find']);
        Route::get('/{id}', [OrientacionController::class, 'get']);
    });

    Route::group(['prefix' => 'tipo-lineas'], function () {
        Route::post('/', [TipoLineaController::class, 'store']);
        Route::put('/', [TipoLineaController::class,'update']);
        Route::post('/delete', [TipoLineaController::class, 'delete']);
        Route::get('/find', [TipoLineaController::class, 'find']);
        Route::get('/{id}', [TipoLineaController::class, 'get']);
    });

    Route::group(['prefix' => 'tipos'], function () {
        Route::post('/', [TipoController::class, 'store']);
        Route::put('/', [TipoController::class,'update']);
        Route::post('/delete', [TipoController::class, 'delete']);
        Route::get('/find', [TipoController::class, 'find']);
        Route::get('/{id}', [TipoController::class, 'get']);
    });

    Route::group(['prefix' => 'unidad-de-medida'], function () {
        Route::post('/', [UnidadMedidaController::class, 'store']);
        Route::put('/', [UnidadMedidaController::class,'update']);
        Route::post('/delete', [UnidadMedidaController::class, 'delete']);
        Route::get('/find', [UnidadMedidaController::class, 'find']);
        Route::get('/{id}', [UnidadMedidaController::class, 'get']);
    });

    Route::group(['prefix' => 'usos'], function () {
        Route::post('/', [UsoController::class, 'store']);
        Route::put('/', [UsoController::class,'update']);
        Route::post('/delete', [UsoController::class, 'delete']);
        Route::get('/find', [UsoController::class, 'find']);
        Route::get('/{id}', [UsoController::class, 'get']);
    });

    Route::group(['prefix' => 'colindancia'], function () {
        Route::post('/', [ColindanciaController::class, 'store']);
        Route::put('/', [ColindanciaController::class,'update']);
        Route::post('/delete', [ColindanciaController::class, 'delete']);
        Route::get('/find', [ColindanciaController::class, 'find']);
        Route::get('/{id}', [ColindanciaController::class, 'get']);
    });

    Route::group(['prefix' => 'clasificacion'], function () {
        Route::post('/', [ClasificacionController::class, 'store']);
        Route::put('/', [ClasificacionController::class,'update']);
        Route::post('/delete', [ClasificacionController::class, 'delete']);
        Route::get('/find', [ClasificacionController::class, 'find']);
        Route::get('/{id}', [ClasificacionController::class, 'get']);
    });

    Route::group(['prefix' => 'subscripcion'], function () {
        Route::post('/', [SubscripcionController::class, 'store']);
        Route::put('/', [SubscripcionController::class,'update']);
        Route::post('/delete', [SubscripcionController::class, 'delete']);
        Route::get('/find', [SubscripcionController::class, 'find']);
        Route::get('/{id}', [SubscripcionController::class, 'get']);
        Route::post('/comprarpaquete', [SubscripcionController::class, 'comprarpaquete']);
    });
});

Route::group(['prefix' => 'usuarios'], function () {
    Route::post('/', [UsuarioController::class, 'store']);
    Route::put('/', [UsuarioController::class,'update']);
    Route::put('/password', [UsuarioController::class,'password']);
    Route::post('/delete', [UsuarioController::class, 'delete']);
    Route::get('/find', [UsuarioController::class, 'find']);
    Route::get('/{id}', [UsuarioController::class, 'get']);
});

Route::group(['prefix' => 'paquetes'], function () {
    Route::post('/', [UsuarioController::class, 'store']);
    Route::put('/', [UsuarioController::class,'update']);
    Route::post('/delete', [UsuarioController::class, 'delete']);
    Route::get('/find', [UsuarioController::class, 'find']);
    Route::get('/{id}', [UsuarioController::class, 'get']);
});

Route::group(['prefix' => 'clientes'], function () {
    Route::get('/find', [ClienteController::class, 'find']);
});

Route::group(['prefix' => 'inmuebles'], function () {
    Route::post('/', [InmuebleController::class, 'store']);
    Route::post('/generacion', [InmuebleController::class, 'generacion']);
    Route::put('/', [InmuebleController::class,'update']);
    Route::post('/delete', [InmuebleController::class, 'delete']);
    Route::get('/find', [InmuebleController::class, 'find']);
    Route::get('/{id}', [InmuebleController::class, 'get']);
});

Route::group(['prefix' => 'areas'], function () {
    Route::post('/', [AreaControler::class, 'store']);
    Route::put('/', [AreaControler::class,'update']);
    Route::post('/delete', [AreaControler::class, 'delete']);
    Route::post('/getCodigo', [AreaControler::class, 'getCodigo']);
    Route::post('/particulares', [AreaControler::class, 'saveParticulares']);
    Route::get('/find', [AreaControler::class, 'find']);
    Route::get('/particulares', [AreaControler::class, 'getParticulares']);
    Route::get('/catalogos/{id}', [AreaControler::class, 'getCatalogos']);
    Route::get('/colindancias/{id}', [AreaControler::class, 'getColindancias']);
    Route::get('/validacion', [AreaControler::class, 'validacion']);
    Route::get('/{id}', [AreaControler::class, 'get']);
    
});

